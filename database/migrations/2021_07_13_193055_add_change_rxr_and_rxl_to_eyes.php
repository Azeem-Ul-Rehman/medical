<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Driver\PDOMySql\Driver;

class AddChangeRxrAndRxlToEyes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eyes', function (Blueprint $table) {
            $table->text('rx_r')->nullable()->change();;
            $table->text('rx_l')->nullable()->change();;
            $table->text('add')->nullable()->change();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eyes', function (Blueprint $table) {
            $table->dropColumn('rx_r');
            $table->dropColumn('rx_l');
            $table->dropColumn('add');
        });
    }
}
