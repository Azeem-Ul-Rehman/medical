<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('email')->nullable();
            $table->text('address')->nullable();
            $table->text('city')->nullable();
            $table->text('postal_code')->nullable();
            $table->text('phone')->nullable();
            $table->text('xm_date')->nullable();
            $table->text('rx_r')->nullable();
            $table->text('rx_l')->nullable();
            $table->text('add')->nullable();
            $table->text('age')->nullable();
            $table->text('birth_date')->nullable();
            $table->text('work_phone')->nullable();
            $table->text('comments')->nullable();
            $table->text('area_code')->nullable();
            $table->text('day')->nullable();
            $table->text('month')->nullable();
            $table->text('year')->nullable();
            $table->text('rx_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
