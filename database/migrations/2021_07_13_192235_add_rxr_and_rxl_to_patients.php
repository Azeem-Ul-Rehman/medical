<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRxrAndRxlToPatients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->text('rx_r_one')->nullable();
            $table->text('rx_r_two')->nullable();
            $table->text('rx_l_one')->nullable();
            $table->text('rx_l_two')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('rx_r_one');
            $table->dropColumn('rx_r_two');
            $table->dropColumn('rx_l_one');
            $table->dropColumn('rx_l_two');
        });
    }
}
