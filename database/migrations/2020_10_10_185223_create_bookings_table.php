<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('email');
            $table->integer('doctor_id');
            $table->string('time');
            $table->string('date');
            $table->integer('status')->default(0);
            $table->text('first_name');
            $table->text('last_name');
            $table->text('address');
            $table->text('birth_date');
            $table->text('contact')->nullable();
            $table->text('type')->nullable();
            $table->text('area_code')->nullable();
            $table->text('number')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
