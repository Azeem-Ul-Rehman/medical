<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if (Auth::check()) {
        $user = Auth::user();
        if ($user->status == 'suspended') {
            Auth::logout();
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'Your Account has been suspended.Please Contact Support.'
            ]);
        } else {

            if ($user->hasRole('doctor')) {
                return redirect()->route('doctor.dashboard.index');
            }
            if ($user->hasRole('admin')) {
                return redirect()->route('admin.dashboard.index');
            }
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);


        }
    } else {
        return view('auth.login');
    }

})->name('index');
//

// Home Page Routes
//Route::get('/', 'FrontendController@index')->name('index');
Route::get('/new-appointment/{doctorId}/{date}', 'FrontendController@show')->name('create.appointment');
Route::post('/book/appointment', 'FrontendController@store')->name('book.appointment');

Auth::routes(['verify' => true]);
Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    return 'Commands run successfully Cleared.';
});


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('social/{provide}', 'Auth\LoginController@redirectToProvider')->name('sociallogin');
Route::get('social/{provide}/callback', 'Auth\LoginController@handleProviderCallback')->name('socialCallBack');


Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/dashboard', 'Backend\DashboardController');
        Route::resource('/eyes', 'Backend\EyesController');
        Route::resource('/areacodes', 'Backend\AreaCodeController');
        Route::resource('/locations', 'Backend\LocationController');
        Route::resource('/users', 'Backend\UserController');
    });
    //Doctor Routes
    Route::prefix('doctor')->name('doctor.')->group(function () {
        Route::get('/dashboard', 'Doctor\MainController@index')->name('dashboard.index');
        Route::get('/get/patient/data', 'Doctor\PatientController@getPatientData')->name('patient.ajax.data');
        Route::resource('/patient', 'Doctor\PatientController');
        Route::post('/patient/import/file', 'Doctor\PatientController@importFile')->name('patient.import.excel');

        Route::post('send/email', 'Doctor\PatientController@sendEmail')->name('send.email');
        Route::post('/patient/bulk/delete', 'Doctor\PatientController@bulkDelete')->name('patient.bulk.delete');

        Route::get('/print/recall/letterhead/one/{patient_id}', 'Doctor\PatientController@printDocumentOne')->name('patient.print.recall.letterhead.one');
        Route::get('/print/recall/letterhead/two/{patient_id}', 'Doctor\PatientController@printDocumentTwo')->name('patient.print.recall.letterhead.two');

        Route::get('/fellow/patients', 'Doctor\PatientController@fetchPatientData')->name('fellow.patient.index');
        Route::get('/fetch/patients', 'Doctor\PatientController@fetchFetchData')->name('fellow.patient.ajax.data');
        Route::get('/fetch/patients/{id}/edit', 'Doctor\PatientController@FellowEdit')->name('fellow.patient.edit');
        Route::post('/fetch/patients/{id}/update', 'Doctor\PatientController@FellowUpdate')->name('fellow.patient.update');

        Route::resource('appointment', 'Doctor\AppointmentController');
        Route::post('/appointment/check', 'Doctor\AppointmentController@check')->name('appointment.check');
        Route::post('/appointment/update', 'Doctor\AppointmentController@updateTime')->name('appointment.check.update');

        Route::get('/bookings', 'Doctor\BookingController@index')->name('bookings.index');
        Route::get('/bookings/status/update/{id}', 'Doctor\BookingController@toggleStatus')->name('bookings.update.status');

        // Labeling Patients
        Route::get('/labeling/patients', 'Doctor\PatientController@fetchLabelingPatientData')->name('labeling.patient.index');
        Route::get('/labeling/fetch/patients', 'Doctor\PatientController@fetchLabelingFetchData')->name('labeling.patient.ajax.data');

    });

    Route::prefix('ajax')->name('ajax.')->group(function () {
        Route::post('/notification/is-read', 'AjaxController@isRead')->name('notification.is-read');

    });
});






