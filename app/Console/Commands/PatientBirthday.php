<?php

namespace App\Console\Commands;


use App\Mail\BirthdayEmail;
use App\Models\Notification;
use App\Models\Patient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;


class PatientBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patient:birthday';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send birthday email to patient.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentDate = date('m-d');
        $patients = Patient::where('birth_date', 'like', '%' . $currentDate . '')->get();
        if (!empty($patients) && count($patients) > 0) {
            foreach ($patients as $patient) {
                if (!is_null($patient->email) && $patient->email != '') {
                    Notification::create([
                        'patient_id' => $patient->id
                    ]);
                    Mail::to($patient->email)->send(new BirthdayEmail($patient, $patient->user->email));
                }

            }
        }
        return true;
    }
}
