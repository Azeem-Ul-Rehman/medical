<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;

class AjaxController extends Controller
{

    public function isRead(Request $request)
    {
        $response = array('status' => '', 'data' => []);
        $notication = Notification::where('id', (int)$request->notification_id)->first();

        $notication->is_read = true;
        $notication->save();
        $response['status'] = 'success';
        $response['action'] = 'true';
        $response['data'] = $notication->id;

        return $response;
    }

}
