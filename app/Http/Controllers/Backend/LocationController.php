<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;


class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $locations = Location::orderBy('id', 'DESC')->get();

        return view('backend.locations.index', compact('locations'));
    }

    public function create()
    {
        return view('backend.locations.create');

    }

    public function store(Request $request)
    {
        $location = new Location();
        $location->location = $request->location ?? 'null';
        $location->save();
        return redirect()->route('admin.locations.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Location created successfully.'
            ]);

    }

    public function edit($id)
    {
        $location = Location::find($id);
        return view('backend.locations.edit', compact('location'));
    }

    public function update(Request $request, $id)
    {
        $location = Location::find($id);
        $location->location = $request->location ?? 'null';
        $location->save();

        return redirect()->route('admin.location.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Location updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $location = Location::findOrFail($id);
        $location->delete();

        return redirect()->route('admin.location.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Location has been deleted'
            ]);
    }
}
