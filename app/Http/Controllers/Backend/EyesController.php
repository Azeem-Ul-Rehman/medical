<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Eye;
use Illuminate\Http\Request;

class EyesController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $eyes = Eye::orderBy('id', 'DESC')->get();

        return view('backend.eyes.index', compact('eyes'));
    }

    public function create()
    {
        return view('backend.eyes.create');

    }

    public function store(Request $request)
    {
        $eye = new Eye();
        $eye->rx_r      = $request->rx_r ?? 'null';
        $eye->rx_r_one  = $request->rx_r_one;
        $eye->rx_r_two  = $request->rx_r_two;
        $eye->rx_l      = $request->rx_l ?? 'null';
        $eye->rx_l_one  = $request->rx_l_one;
        $eye->rx_l_two  = $request->rx_l_two;
        $eye->add       = $request->add ?? 'null';
        $eye->save();
        return redirect()->route('admin.eyes.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Eye created successfully.'
            ]);

    }

    public function edit($id)
    {
        $eye = Eye::find($id);
        return view('backend.eyes.edit', compact('eye'));
    }

    public function update(Request $request, $id)
    {
        $eye= Eye::find($id);
        $eye->rx_r      = $request->rx_r ?? 'null';
        $eye->rx_r_one  = $request->rx_r_one;
        $eye->rx_r_two  = $request->rx_r_two;
        $eye->rx_l      = $request->rx_l ?? 'null';
        $eye->rx_l_one  = $request->rx_l_one;
        $eye->rx_l_two  = $request->rx_l_two;
        $eye->add       = $request->add ?? 'null';
        $eye->save();

        return redirect()->route('admin.eyes.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Eye updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $eye = Eye::findOrFail($id);
        $eye->delete();

        return redirect()->route('admin.eyes.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Eye has been deleted'
            ]);
    }

}
