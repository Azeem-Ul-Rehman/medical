<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;


use App\Traits\GeneralHelperTrait;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;



class AjaxController extends Controller
{
    use GeneralHelperTrait;

    public function example(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
        ], [

        ]);

        if (!$validator->fails()) {


            $response['status'] = 'success';
            $response['data'] = [

            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }



}
