<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->where('user_type', 'doctor')->get();
        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        return view('backend.users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:2048',

        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'email.required' => 'Email is required.',
        ]);


        $role = Role::find(2);

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/signature');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $e_signature = $name;
        } else {

            $e_signature = 'default.png';
        }
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role_id' => $role->id,
            'user_type' => $role->name,
            'status' => 'verified',
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'e_signature'=>$e_signature
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User created successfully.'
            ]);
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('backend.users.edit', compact('user'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|max:255',
            'image' => 'image|mimes:jpg,jpeg,png|max:2048',
        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'email.required' => 'Email is required.',

        ]);

        $user = User::find($id);


        $role = Role::find(2);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/signature');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $e_signature = $name;
        } else {
            $e_signature = $user->e_signature;
        }
        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role_id' => $role->id,
            'user_type' => $role->name,
            'status' => 'verified',
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'e_signature' => $e_signature,
        ]);
        (new \App\Models\UserRole)->update([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User has been deleted'
            ]);
    }

}
