<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $doctors = User::where('user_type', 'doctor')->get();
        return view('backend.dashboard.index',compact('doctors'));
    }
}
