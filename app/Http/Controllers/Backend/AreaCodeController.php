<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\AreaCode;
use Illuminate\Http\Request;

class AreaCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $areacodes = AreaCode::orderBy('id', 'DESC')->get();

        return view('backend.areacodes.index', compact('areacodes'));
    }

    public function create()
    {
        return view('backend.areacodes.create');

    }

    public function store(Request $request)
    {
        $areacode = new AreaCode();
        $areacode->code = $request->code ?? 'null';
        $areacode->save();
        return redirect()->route('admin.areacodes.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Area Code created successfully.'
            ]);

    }

    public function edit($id)
    {
        $areacode = AreaCode::find($id);
        return view('backend.areacodes.edit', compact('areacode'));
    }

    public function update(Request $request, $id)
    {
        $areacode = AreaCode::find($id);
        $areacode->code = $request->code ?? 'null';
        $areacode->save();

        return redirect()->route('admin.areacode.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Area Code updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $areacode = AreaCode::findOrFail($id);
        $areacode->delete();

        return redirect()->route('admin.areacode.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Area Code has been deleted'
            ]);
    }
}
