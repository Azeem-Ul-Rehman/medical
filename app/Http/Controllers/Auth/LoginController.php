<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\UserRole;
use App\Providers\RouteServiceProvider;
use App\Models\Activation;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function username()
    {
        return 'email';
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->status == 'suspended') {
            Auth::logout();
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'Your Account has been suspended.Please Contact Support.'
            ]);
        } else {

            if ($user->hasRole('doctor')) {
                return redirect()->route('doctor.dashboard.index');
            }
            if ($user->hasRole('admin')) {
                return redirect()->route('admin.dashboard.index');
            }


            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);


        }


    }

    public function redirectToProvider($provider)
    {
        session()->put('from', url()->previous());
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback(Request $request, $provider)
    {
        $userSocial = Socialite::driver($provider)->user();

        $user = User::where(['email' => $userSocial->getEmail()])->first();
        if ($user) {
            Auth::login($user);
        } else {
            $role = Role::find(2);
            $user = User::create([
                'first_name' => $userSocial->getName(),
                'last_name' => $userSocial->getName(),
                'username' => $userSocial->getName(),
                'role_id' => $role->id,
                'user_type' => $role->name,
                'status' => 'verified',
                'email' => $userSocial->getEmail(),
                'password' => Hash::make(123456789),
            ]);

            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);
            Auth::login($user);


        }

        if ($user->user_type == 'doctor') {
            return redirect()->route('doctor.dashboard.index');
        }
    }

}
