<?php

namespace App\Http\Controllers;

use App\Models\AreaCode;
use App\Models\Booking;
use App\Mail\AppointmentMail;
use App\Models\Appointment;
use App\Models\Patient;
use App\Models\PatientContact;
use App\Models\Time;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index(Request $request)
    {
        // If there is set date, find the doctors
        if (request('date')) {
            $formatDate = date('Y-m-d', strtotime(request('date')));
            $doctors = Appointment::where('date', $formatDate)->get();
            return view('welcome', compact('doctors', 'formatDate'));
        };
        // Return all doctors avalable for today to the welcome page
        $doctors = Appointment::where('date', date('Y-m-d'))->get();

        return view('welcome', compact('doctors'));
    }

    public function show($doctorId, $date)
    {
        $appointment = Appointment::where('doctor_id', $doctorId)->where('date', $date)->first();
        $times = Time::where('appointment_id', $appointment->id)->where('status', 0)->get();
        $user = User::where('id', $doctorId)->first();
        $doctor_id = $doctorId;
        $areacodes = AreaCode::all();
        return view('appointment', compact('times', 'date', 'user', 'doctor_id', 'areacodes'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'time' => 'required',
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'birth_date' => 'required',
            'contact' => 'required',
            'type' => 'required',
            'area_code' => 'required',
            'number' => 'required',
        ]);
        $check = $this->checkBookingTimeInterval($request->email);
        if ($check) {
            return redirect()->back()->with('errMessage', 'You already made an appointment. Please contact doctor for furture appointments.');
        }

        $doctorId = $request->doctorId;
        $time = $request->time;
        $appointmentId = $request->appointmentId;
        $date = $request->date;
        Booking::create([
            'email' => $request->email,
            'doctor_id' => $doctorId,
            'time' => $time,
            'date' => $date,
            'status' => 0,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'address' => $request->address,
            'birth_date' => $request->birth_date,
            'contact' => $request->contact,
            'type' => $request->type,
            'area_code' => $request->area_code,
            'number' => $request->number,
        ]);

        $this->storePatient($request, $doctorId);
        $doctor = User::where('id', $doctorId)->first();
        Time::where('appointment_id', $appointmentId)->where('time', $time)->update(['status' => 1]);

        // Send email notification
        $mailData = [
            'name' => $request->first_name . ' ' . $request->last_name,
            'time' => $time,
            'date' => $date,
            'doctorName' => $doctor->fullName(),
            'location' => $request->location
        ];
        try {
            Mail::to($request->email)->send(new AppointmentMail($mailData));
        } catch (\Exception $e) {
        }

        return redirect()->back()->with('message', 'Your appointment was booked for ' . $date . ' at ' . $time . ' with ' . $doctor->fullName() . '.');
    }

    // check if user already make a booking.
    public function checkBookingTimeInterval($email)
    {
        return Booking::orderby('id', 'desc')
            ->where('email', $email)
            ->exists();
    }

    //store Patient Record
    public function storePatient($request, $doctorId)
    {
        $patient = new Patient();
        $patient->user_id = $doctorId;
        $patient->title = 'Mr.';
        $patient->first_name = $request->get('first_name');
        $patient->last_name = $request->get('last_name');
        $patient->email = $request->get('email');
        $patient->address = $request->get('address');
        $patient->birth_date = $request->get('birth_date');
        $patient->location = $request->get('location');
        $patient->is_recall = 'no';
        $patient->save();

        if (!is_null($request->number)) {
            $contact = new PatientContact();
            $contact->patient_id = $patient->id;
            $contact->contact = $request->contact;
            $contact->type = $request->type;
            $contact->area_code = $request->area_code;
            $contact->number = $request->number;
            $contact->save();
        }
    }
}
