<?php

namespace App\Http\Controllers\Doctor;

use DataTables;
use App\Http\Controllers\Controller;
use App\Imports\PatientImport;
use App\Mail\Reminder;
use App\Models\AreaCode;
use App\Models\Eye;
use App\Models\Location;
use App\Models\Patient;
use App\Models\User;
use App\Models\PatientContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:doctor');
    }

    public function index(Request $request)
    {
//        $patients = Patient::whereNotNull('id');
//
//        if (!is_null($request->is_recall)) {
//            $patients->where('is_recall', $request->is_recall);
//
//        }
//        $patients = $patients->orderBy('id', 'asc')->where('user_id', Auth::id())->with('primaryContact')->get();

        return view('doctor.patient.index');
    }

    public function getPatientData(Request $request)
    {
        $patients = Patient::with('primaryContact', 'user')->whereHas('user')->select([
            'patients.id',
            'patients.user_id',
            'patients.email',
            'patients.first_name',
            'patients.last_name',
            'patients.comanaged_by',
            'patients.is_recall',
            'patients.location',
            'patients.title',
            'patients.address',
            'patients.city',
            'patients.postal_code',
            'patients.xm_date',
            'patients.rx_r',
            'patients.rx_l',
            'patients.add',
            'patients.birth_date',
            'patients.comments',
            'patients.rx_date',
        ]);
        $user_id = Auth::id();
        return Datatables::of($patients)
            ->filter(function ($query) use ($request,$user_id) {
                if (isset($request->search['value']) && !is_null($request->search['value'])) {
                    $query->where('patients.user_id', 'like', "{$user_id}")->where('first_name', 'like', '%' . $request->search['value'] . '%')->orWhere('last_name', 'like', '%' . $request->search['value'] . '%');
                }
                if ($request->has('is_recall') && !empty($request->is_recall)) {
                    $query->where('patients.user_id', 'like', "{$user_id}")->where('patients.is_recall', 'like', "{$request->get('is_recall')}");
                }
            })
            ->addColumn('name', function ($patients) {
                return $patients->first_name . ' ' . $patients->last_name;
            })
            ->addColumn('doctor_name', function ($patients) {
                return $patients->user->fullName();
            })
            ->addColumn('comanaged_by', function ($patients) {
                return $patients->comanaged_by ? ucfirst($patients->comanaged_by) : 'N/A';
            })
            ->addColumn('is_recall', function ($patients) {
                return $patients->is_recall ? ucfirst($patients->is_recall) : 'NO';
            })
            ->addColumn('location', function ($patients) {
                return $patients->location ?? 'N/A';
            })
            ->addColumn('title', function ($patients) {
                return $patients->title ?? 'N/A';
            })
            ->addColumn('address', function ($patients) {
                return $patients->address ?? 'N/A';
            })
            ->addColumn('city', function ($patients) {
                return $patients->city ?? 'N/A';
            })
            ->addColumn('postal_code', function ($patients) {
                return $patients->postal_code ?? 'N/A';
            })
            ->addColumn('xm_date', function ($patients) {
                return $patients->xm_date ?? 'N/A';
            })
            ->addColumn('rx_r', function ($patients) {
                return $patients->rx_r ?? 'N/A';
            })
            ->addColumn('rx_l', function ($patients) {
                return $patients->rx_l ?? 'N/A';
            })
            ->addColumn('add', function ($patients) {
                return $patients->add ?? 'N/A';
            })
            ->addColumn('birth_date', function ($patients) {
                return $patients->birth_date ?? 'N/A';
            })
            ->addColumn('comments', function ($patients) {
                return $patients->comments ?? 'N/A';
            })
            ->addColumn('rx_date', function ($patients) {
                return $patients->rx_date ?? 'N/A';
            })
            ->addColumn('primary_contact', function ($patients) {
                if (!is_null($patients->primaryContact)) {
                    return $patients->primaryContact->area_code . '-' . $patients->primaryContact->number;
                } else {
                    return 'N/A';
                }

            })
            ->addColumn('action', function ($patients) {

                if (strtolower($patients->is_recall) == 'yes') {
                    $display = '';
                } else {
                    $display = 'none';
                }
                return '<a href="' . route('doctor.patient.edit', $patients->id) . '"
                                       style="margin: 10px;"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>

                            <form method="post"
                              action="' . route('doctor.patient.destroy', $patients->id) . '"
                              id="delete_' . $patients->id . '">
                                <input type="hidden" name="_token" value="' . csrf_token() . '">
                            ' . method_field("delete") . '

                                <a style="margin-left:10px;" class="btn btn-sm btn-danger m-left"
                                   href="javascript:void(0)"
                                   onclick="confirmDeletePatient(' . $patients->id . ')">
                                Delete
                                </a>
                            </form>
                             <a href="javascript:void(0)" data-id="' . $patients->id . '" style="display: ' . $display . '"
                                   data-email="' . $patients->email . '"
                                   data-doctor-email="' . Auth::user()->email . '"
                                   style="margin-left:10px; margin-top: 10px" id="sendEmail"
                                   class="btn btn-sm btn-danger m-left">Send Reminder</a>

                            <a href="javascript:void(0)" data-id="' . $patients->id . '"
                               style="margin-left:10px; margin-top: 10px" id="printDocument"
                               class="btn btn-sm btn-darker m-left">Print</a>';
            })
            ->addColumn('checkbox', function ($patients) {
                return '<input type="checkbox" class="delete_check" id="delcheck_' . $patients->id . '"
                                                   onclick="checkcheckbox();" value="' . $patients->id . '">';
            })
            ->rawColumns(['checkbox', 'action', 'doctor_name', 'comanaged_by', 'is_recall', 'location', 'title', 'name', 'email', 'address', 'city', 'postal_code', 'primary_contact', 'xm_date', 'rx_r', 'rx_l', 'add', 'birth_date', 'comments', 'rx_date'])
            ->setRowId(function ($users) {
                return 'user_dt_row_' . $users->id;
            })
            ->make(true);
    }


    public function create()
    {
        $eyes = Eye::all();
        $areacodes = AreaCode::all();
        $locations = Location::all();
        $users = User::where('id', '!=', 1)->get(['id', 'first_name', 'last_name']);
        return view('doctor.patient.create', compact('eyes', 'areacodes', 'locations', 'users'));
    }

    public function store(Request $request)
    {
//        $this->validate($request, [
//            'title' => 'required',
//            'first_name' => 'required',
//            'last_name' => 'required',
//            'email' => 'required|email|max:255|unique:patients',
//            'address' => 'required',
//            'city' => 'required',
//            'postal_code' => 'required',
//            'phone' => 'required',
//            'xm_date' => 'required',
//            'add' => 'required',
////            'age' => 'required',
//            'birth_date' => 'required',
////            'work_phone' => 'required',
//            'comments' => 'required',
//            'area_code' => 'required',
////            'day' => 'required',
////            'month' => 'required',
////            'year' => 'required',
//            'rx_date' => 'required',
//            'location' => 'required',
//        ]);


        $patient = new Patient();
        $patient->user_id = $request->user_id;
        $patient->title = $request->get('title');
        $patient->first_name = $request->get('first_name');
        $patient->last_name = $request->get('last_name');
        $patient->email = $request->get('email');
        $patient->address = $request->get('address');
        $patient->city = $request->get('city');
        $patient->postal_code = $request->get('postal_code');
        $patient->xm_date = $request->get('xm_date');
        $patient->rx_r = $request->rx_r;
        $patient->rx_l = $request->rx_l;
        $patient->add = $request->get('add');
        $patient->birth_date = $request->get('birth_date');
        $patient->comments = $request->get('comments');
        $patient->location = $request->get('location');
        $patient->rx_date = $request->get('rx_date');
        $patient->is_recall = $request->get('is_recall');
        $patient->comanaged_by = $request->get('comanaged_by');
        $patient->save();
        if (!empty($request->number) && count($request->number) > 0) {
            foreach ($request->number as $key => $number) {
                $contact = new PatientContact();
                $contact->patient_id = $patient->id;
                $contact->contact = $request->contact[$key];
                $contact->type = $request->type[$key];
                $contact->area_code = $request->area_code[$key];
                $contact->number = $number;
                $contact->save();
            }
        }


        return redirect()->route('doctor.patient.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Patient created successfully.'
            ]);
    }

    public function edit($id)
    {
        $patient = Patient::where('id', $id)->with('patientContacts')->first();
        $eyes = Eye::all();
        $areacodes = AreaCode::all();
        $locations = Location::all();
        $users = User::where('id', '!=', 1)->get(['id', 'first_name', 'last_name']);
        return view('doctor.patient.edit', compact('patient', 'eyes', 'areacodes', 'locations', 'users'));


    }

    public function update(Request $request, $id)
    {


        $patient = Patient::findOrFail($id);
        $patient->user_id = $request->user_id;
        $patient->title = $request->get('title');
        $patient->first_name = $request->get('first_name');
        $patient->last_name = $request->get('last_name');
        $patient->email = $request->get('email');
        $patient->address = $request->get('address');
        $patient->city = $request->get('city');
        $patient->postal_code = $request->get('postal_code');
        $patient->xm_date = $request->get('xm_date');
        $patient->rx_r = $request->rx_r;
        $patient->rx_l = $request->rx_l;
        $patient->add = $request->get('add');
        $patient->birth_date = $request->get('birth_date');
        $patient->comments = $request->get('comments');
        $patient->location = $request->get('location');
        $patient->rx_date = $request->get('rx_date');
        $patient->is_recall = $request->get('is_recall');
        $patient->comanaged_by = $request->get('comanaged_by');
        $patient->save();
        if (!empty($request->number) && count($request->number) > 0) {
            PatientContact::where('patient_id', $id)->delete();
            foreach ($request->number as $key => $number) {
                $contact = new PatientContact();
                $contact->patient_id = $patient->id;
                $contact->contact = $request->contact[$key];
                $contact->type = $request->type[$key];
                $contact->area_code = $request->area_code[$key];
                $contact->number = $number;
                $contact->save();
            }
        }


        return redirect()->route('doctor.patient.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Patient updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $patient = Patient::findOrFail($id);
        $patient->delete();

        return redirect()->route('doctor.patient.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Patient has been deleted'
            ]);
    }

    public function bulkDelete(Request $request)
    {
        $deleteids_arr = $request->get('deleteids_arr');
        foreach ($deleteids_arr as $deleteid) {
            $patient = Patient::findOrFail($deleteid);
            $patient->delete();
        }
        return 'success';
    }


    public function importFile(Request $request)
    {
        ini_set('upload_max_filesize', '20m');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx,csv'
        ]);
        if (!$validator->fails()) {
            $deleteRecord = Patient::where('user_id', Auth::id())->delete();
            $data = Excel::import(new PatientImport(), $request->file('file'));
            $status = 'success';
            $message = 'Patient File Imported Successfully.';
        } else {
            $status = 'error';
            $message = 'File Format is not correct or Incorrect Data.';
        }

        return redirect()->route('doctor.patient.index')
            ->with([
                'flash_status' => $status,
                'flash_message' => $message
            ]);
    }

    public function sendEmail(Request $request)
    {


        $patient = Patient::where('id', $request->patient_id)->first();
        $from = Auth::user()->email;
        $to = $request->patient_email ? $request->patient_email : $patient->email;


        if ($request->has('file')) {
            $image = $request->file('file');
            $var = date_create();
            $time = date_format($var, 'YmdHis');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/files');
            $attachement = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
        } else {

            $attachement = null;
            $name = null;
        }


        $data = [
            'message' => $request->message
        ];
        Mail::to($to)->send(new Reminder($patient, $from, $request->subject, $data, $attachement, $name));

        return redirect()->route('doctor.patient.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Reminder sent successfully.'
            ]);

    }

    public function printDocumentOne($patientId)
    {

        $patient = Patient::where('id', $patientId)->first();
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/letterhead/recall_letterhead_one.docx'));
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);

        $my_template->setValue('first_name', $patient->first_name);
        $my_template->setValue('last_name', $patient->last_name);
        $my_template->setValue('address', $patient->address);
        $my_template->setValue('city', $patient->city);
        $my_template->setValue('province', $patient->location);
        $my_template->setValue('postal_code', $patient->postal_code);
        $my_template->setValue('xm_date', $patient->xm_date);
        try {
            $my_template->saveAs(public_path('uploads/finalLetterHead/print.docx'));
        } catch (Exception $e) {
            //handle exception
        }


        return response()->download(public_path('uploads/finalLetterHead/print.docx'));

    }

    public function printDocumentTwo($patientId)
    {
        $patient = Patient::where('id', $patientId)->first();
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/letterhead/recall_letterhead_two.docx'));
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);

        $my_template->setValue('first_name', $patient->first_name);
        $my_template->setValue('last_name', $patient->last_name);
        $my_template->setValue('address', $patient->address);
        $my_template->setValue('city', $patient->city);
        $my_template->setValue('province', $patient->location);
        $my_template->setValue('postal_code', $patient->postal_code);
        $my_template->setValue('xm_date', $patient->xm_date);
        try {
            $my_template->saveAs(public_path('uploads/finalLetterHead/print.docx'));
        } catch (Exception $e) {
            //handle exception
        }


        return response()->download(public_path('uploads/finalLetterHead/print.docx'));

    }


    public function fetchPatientData(Request $request)
    {
        $doctors = User::orderBy('id', 'DESC')->where('user_type', 'doctor')->where('id', '!=', Auth::id())->get(['id', 'first_name', 'last_name']);
        return view('doctor.fellow-patient.index', compact('doctors'));
    }

    public function fetchFetchData(Request $request)
    {
        $patients = Patient::with('primaryContact', 'user')->whereHas('user')->select([
            'patients.id',
            'patients.user_id',
            'patients.email',
            'patients.first_name',
            'patients.last_name',
            'patients.comanaged_by',
            'patients.is_recall',
            'patients.location',
            'patients.title',
            'patients.address',
            'patients.city',
            'patients.postal_code',
            'patients.xm_date',
            'patients.rx_r',
            'patients.rx_l',
            'patients.add',
            'patients.birth_date',
            'patients.comments',
            'patients.rx_date',
        ]);
        return Datatables::of($patients)
            ->filter(function ($query) use ($request) {

                if (isset($request->search['value']) && !is_null($request->search['value'])) {
                    $query->where('patients.user_id', 'like', "{$request->get('user_id')}")->where('first_name', 'like', '%' . $request->search['value'] . '%')->orWhere('last_name', 'like', '%' . $request->search['value'] . '%');
                }
                if ($request->has('user_id') && !empty($request->user_id)) {
                    $query->where('patients.user_id', 'like', "{$request->get('user_id')}")->whereHas('user', function ($q) use ($request) {
                        $q->where('id', "{$request->get('user_id')}");
                    });
                }
            })
            ->addColumn('name', function ($patients) {
                return $patients->first_name . ' ' . $patients->last_name;
            })
            ->addColumn('doctor_name', function ($patients) {
                return $patients->user->fullName();
            })
            ->addColumn('comanaged_by', function ($patients) {
                return $patients->comanaged_by ? ucfirst($patients->comanaged_by) : 'N/A';
            })
            ->addColumn('is_recall', function ($patients) {
                return $patients->is_recall ? ucfirst($patients->is_recall) : 'NO';
            })
            ->addColumn('location', function ($patients) {
                return $patients->location ?? 'N/A';
            })
            ->addColumn('title', function ($patients) {
                return $patients->title ?? 'N/A';
            })
            ->addColumn('address', function ($patients) {
                return $patients->address ?? 'N/A';
            })
            ->addColumn('city', function ($patients) {
                return $patients->city ?? 'N/A';
            })
            ->addColumn('postal_code', function ($patients) {
                return $patients->postal_code ?? 'N/A';
            })
            ->addColumn('xm_date', function ($patients) {
                return $patients->xm_date ?? 'N/A';
            })
            ->addColumn('rx_r', function ($patients) {
                return $patients->rx_r ?? 'N/A';
            })
            ->addColumn('rx_l', function ($patients) {
                return $patients->rx_l ?? 'N/A';
            })
            ->addColumn('add', function ($patients) {
                return $patients->add ?? 'N/A';
            })
            ->addColumn('birth_date', function ($patients) {
                return $patients->birth_date ?? 'N/A';
            })
            ->addColumn('comments', function ($patients) {
                return $patients->comments ?? 'N/A';
            })
            ->addColumn('rx_date', function ($patients) {
                return $patients->rx_date ?? 'N/A';
            })
            ->addColumn('primary_contact', function ($patients) {
                if (!is_null($patients->primaryContact)) {
                    return $patients->primaryContact->area_code . '-' . $patients->primaryContact->number;
                } else {
                    return 'N/A';
                }

            })
            ->addColumn('action', function ($patients) {
                return '<a href="' . route('doctor.fellow.patient.edit', ['id' => $patients->id]) . '"
                                               style="margin: 10px;"
                                               class="btn btn-sm btn-info pull-left ">Edit</a>';
            })
            ->rawColumns(['action', 'doctor_name', 'comanaged_by', 'is_recall', 'location', 'title', 'name', 'email', 'address', 'city', 'postal_code', 'primary_contact', 'xm_date', 'rx_r', 'rx_l', 'add', 'birth_date', 'comments', 'rx_date'])
            ->setRowId(function ($users) {
                return 'user_dt_row_' . $users->id;
            })
            ->make(true);
    }

    public function FellowEdit($id)
    {
        $patient = Patient::where('id', $id)->with('patientContacts')->first();
        $eyes = Eye::all();
        $areacodes = AreaCode::all();
        $locations = Location::all();
        $users = User::where('id', '!=', 1)->get(['id', 'first_name', 'last_name']);
        return view('doctor.fellow-patient.edit', compact('patient', 'eyes', 'areacodes', 'locations', 'users'));


    }

    public function FellowUpdate(Request $request, $id)
    {
        $patient = Patient::findOrFail($id);
        $patient->title = $request->get('title');
        $patient->first_name = $request->get('first_name');
        $patient->last_name = $request->get('last_name');
        $patient->email = $request->get('email');
        $patient->address = $request->get('address');
        $patient->city = $request->get('city');
        $patient->postal_code = $request->get('postal_code');
        $patient->xm_date = $request->get('xm_date');
        $patient->rx_r = $request->rx_r;
        $patient->rx_l = $request->rx_l;
        $patient->add = $request->get('add');
        $patient->birth_date = $request->get('birth_date');
        $patient->comments = $request->get('comments');
        $patient->location = $request->get('location');
        $patient->rx_date = $request->get('rx_date');
        $patient->is_recall = $request->get('is_recall');
        $patient->comanaged_by = $request->get('comanaged_by');
        $patient->save();
        if (!empty($request->number) && count($request->number) > 0) {
            PatientContact::where('patient_id', $id)->delete();
            foreach ($request->number as $key => $number) {
                $contact = new PatientContact();
                $contact->patient_id = $patient->id;
                $contact->contact = $request->contact[$key];
                $contact->type = $request->type[$key];
                $contact->area_code = $request->area_code[$key];
                $contact->number = $number;
                $contact->save();
            }
        }


        return redirect()->route('doctor.fellow.patient.edit', $patient->id)
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Patient updated successfully.'
            ]);
    }

// Labeling Patient

    public function fetchLabelingPatientData(Request $request)
    {
        $doctors = User::orderBy('id', 'DESC')->where('user_type', 'doctor')->get(['id', 'first_name', 'last_name']);
        return view('doctor.labeling-patient.index', compact('doctors'));
    }

    public function fetchLabelingFetchData(Request $request)
    {
        $patients = Patient::with('primaryContact', 'user')->whereHas('user')->select([
            'patients.id',
            'patients.user_id',
            'patients.first_name',
            'patients.last_name',
            'patients.address',
            'patients.postal_code',
        ]);
        $user_id = Auth::id();
        return Datatables::of($patients)
            ->filter(function ($query) use ($request, $user_id) {


                if ($request->has('year') && !empty($request->year)) {
                    $query->where('patients.user_id', 'like', "{$user_id}")->whereYear('patients.created_at', $request->year)->whereHas('user', function ($q) use ($request, $user_id) {
                        $q->where('id', "{$user_id}");
                    });
                }
                if ($request->has('month') && !empty($request->month)) {
                    $query->where('patients.user_id', 'like', "{$user_id}")->whereMonth('patients.created_at', $request->month)->whereHas('user', function ($q) use ($request, $user_id) {
                        $q->where('id', "{$user_id}");
                    });
                }
                if ($request->has('day') && !empty($request->day)) {
                    $query->where('patients.user_id', 'like', "{$user_id}")->whereDay('patients.created_at', $request->day)->whereHas('user', function ($q) use ($request, $user_id) {
                        $q->where('id', "{$user_id}");
                    });
                }
            })
            ->addColumn('first_name', function ($patients) {
                return $patients->first_name;
            })
            ->addColumn('last_name', function ($patients) {
                return $patients->last_name;
            })
            ->addColumn('doctor_name', function ($patients) {
                return $patients->user->fullName();
            })
            ->addColumn('postal_code', function ($patients) {
                return $patients->postal_code ?? 'N/A';
            })
            ->rawColumns(['doctor_name', 'first_name', 'last_name', 'address', 'postal_code'])
            ->setRowId(function ($users) {
                return 'user_dt_row_' . $users->id;
            })
            ->make(true);
    }
}
