<?php

namespace App\Http\Controllers\Doctor;
use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\Time;


class AppointmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:doctor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myAppointments = Appointment::latest()->where('doctor_id', auth()->user()->id)->get();
        return view('doctor.appointment.index', compact('myAppointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::all();
        return view('doctor.appointment.create', compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation, unique:table,column with the column id == user_id
        $this->validate($request, [
//            'date' => 'required|unique:appointments,location,NULL,id,doctor_id,' . \Auth::id(),
            'date' => 'required',
            'location' => 'required',
            'time' => 'required'
        ]);

        $appointment = Appointment::create([
            'doctor_id' => auth()->user()->id,
            'date' => $request->date,
            'location' => $request->location,
        ]);
        foreach ($request->time as $time) {
            Time::create([
                'appointment_id' => $appointment->id,
                'time' => $time
                // default status => 0
            ]);
        }
        return redirect()->back()->with([
            'flash_status' => 'success',
            'flash_message' => 'An appointment created for ' . $request->date
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Check specific date for appointment time
    public function check(Request $request)
    {
        $date = $request->date;
        $appointment = Appointment::where('date', $date)->where('doctor_id', auth()->user()->id)->first();
        if (!$appointment) {
            return redirect()->to('/doctor/appointment')
                ->with([
                    'flash_status' => 'error',
                    'flash_message' => 'Appointment time is not available for this date'
                ]);
        };
        $appointmentId = $appointment->id;
        $times = Time::where('appointment_id', $appointmentId)->get();
        return view('doctor.appointment.index', compact('times', 'appointmentId', 'date'));
    }
    // Update app time
    // delete the old time array and create a new time array
    public function updateTime(Request $request)
    {
        $appointmentId = $request->appointmentId;
        $date = Appointment::where('id', $appointmentId)->get('date')->first()->date;
        Time::where('appointment_id', $appointmentId)->delete();
        foreach ($request->time as $time) {
            Time::create([
                'appointment_id' => $appointmentId,
                'time' => $time,
                'status' => 0
            ]);
        }
        return redirect()->route('doctor.appointment.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Appointment time for ' . $date . ' is updated successfully!'
            ]);
    }
}
