<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Patient;
use Illuminate\Support\Facades\Auth;


class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:doctor');
    }

    public function index()
    {
        $patients = Patient::where('user_id', Auth::id())->count();
        $confirmed_booking = Booking::where('doctor_id', Auth::id())->whereStatus(0)->count();
        $pending_booking = Booking::where('doctor_id', Auth::id())->whereStatus(1)->count();
        return view('doctor.dashboard.index', compact('patients', 'confirmed_booking', 'pending_booking'));
    }

}
