<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Booking;

class BookingController extends Controller
{
    public function index(Request $request)
    {
        // Set timezone
//        date_default_timezone_set('America/New_York');
        if ($request->date) {
            $date = $request->date;
            $bookings = Booking::latest()->where('date', $date)->get();
            return view('doctor.bookings.index', compact('bookings', 'date'));
        }
        $bookings = Booking::latest()->where('date', date('Y-m-d'))->get();
        return view('doctor.bookings.index', compact('bookings'));
    }

    public function toggleStatus($id)
    {
        $booking = Booking::find($id);
        $booking->status = !$booking->status;
        $booking->save();
        return redirect()->back();
    }
}
