<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class BirthdayEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $patient;
    public $fromAddress;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($patient, $fromAddress)
    {
        $this->patient = $patient;
        $this->fromAddress = $fromAddress;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        config()->set([
            'mail.from.address' => $this->fromAddress,
        ]);
        $email = $this->from($this->fromAddress, 'Medical')
            ->subject('Birthday Wishes')->view('mails.birthday');
        return $email;
    }
}
