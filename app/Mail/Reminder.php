<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class Reminder extends Mailable
{
    use Queueable, SerializesModels;

    public $patient;
    public $fromAddress;
    public $subject;
    public $data;
    public $attachment;
    public $name;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($patient, $fromAddress, $subject, $data, $attachment, $name)
    {
        $this->patient = $patient;
        $this->fromAddress = $fromAddress;
        $this->subject = $subject;
        $this->data = $data;
        $this->attachment = $attachment;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $attachments = [
            '' . $this->attachment . ''
        ];
        config()->set([
            'mail.from.address' => $this->fromAddress,
        ]);
        $email = $this->from($address = $this->fromAddress, $name = 'Medical')
            ->subject($this->subject)->view('mails.reminder');
        // $attachments is an array with file paths of attachments
        if (!empty($attachments) && count($attachments) > 0) {
            foreach ($attachments as $file) {
                if ($file != '') {
                    $email->attach($file);
                }

            }
        }

        return $email;
    }
}
