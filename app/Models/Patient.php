<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Patient extends Model
{
    protected $table = 'patients';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function patientContacts()
    {
        return $this->hasMany(PatientContact::class, 'patient_id', 'id')->orderBy('id', 'ASC');
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function primaryContact()
    {
        return $this->hasOne(PatientContact::class)->where('contact', 'Primary');
    }

}
