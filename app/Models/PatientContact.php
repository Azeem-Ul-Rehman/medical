<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientContact extends Model
{
    protected $table = 'patient_contacts';
    protected $guarded = [];

}
