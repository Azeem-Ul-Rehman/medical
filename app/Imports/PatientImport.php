<?php

namespace App\Imports;

use App\Models\Patient;
use App\Models\PatientContact;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PatientImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $data) {

            $row = [];
            foreach ($data as $key => $value) {
                $row[$this->filterHeader($key)] = $value;
            }
            $patient = new Patient();
            $patient->user_id = Auth::id();
            $patient->title = isset($row['title']) ? $row['title'] : 'Mr.';
            $patient->first_name = $row['first_name'];
            $patient->last_name = $row['last_name'];
            $patient->email = $row['email'];
            $patient->address = $row['address'];
            $patient->city = $row['city'];
            $patient->postal_code = $row['postal_code'];
            $patient->xm_date = $row['xm_date'];
            $patient->rx_r = $row['rx_r'];
            $patient->rx_l = $row['rx_l'];
            $patient->add = $row['add'];
            $patient->birth_date = $row['birth_date'];
            $patient->comments = $row['comments'];
            $patient->location = $row['location'];
            $patient->rx_date = $row['rx_date'];
            $patient->is_recall = $row['is_recall'] ? strtolower($row['is_recall']) : 'no';
            $patient->comanaged_by = $row['comanaged_by'];
            $patient->save();
            if (!is_null($row['number'])) {
                $contact = new PatientContact();
                $contact->patient_id = $patient->id;
                $contact->contact = $row['contact'] ?? 'primary';
                $contact->type = $row['type'] ?? 'Phone Number';
                $contact->area_code = $row['area_code'];
                $contact->number = $row['number'];
                $contact->save();
            }

        }
    }

    public function filterHeader($header)
    {
        $setHeader = preg_replace('/[^a-zA-Z0-9_]/', '', str_replace(' ', '_', strtolower($header)));
        return $setHeader;
    }
}
