<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <p>Medical</p>
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/dashboard')) ? 'active' : '' }}"
                           href="{{ route('admin.dashboard.index') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/eyes')) ? 'active' : '' }}"
                           href="{{ route('admin.eyes.index') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Eye</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/areacodes')) ? 'active' : '' }}"
                           href="{{ route('admin.areacodes.index') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Area Codes</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/locations')) ? 'active' : '' }}"
                           href="{{ route('admin.locations.index') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Locations</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/users')) ? 'active' : '' }}"
                           href="{{ route('admin.users.index') }}">
                            <i class="ni ni-single-02 text-primary"></i>
                            <span class="nav-link-text">Users</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>
