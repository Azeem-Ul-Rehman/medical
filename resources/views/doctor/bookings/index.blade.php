@extends('doctor.main')
@section('title','Bookings')
@push('css')
    <link rel="stylesheet" type="text/css"
          href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">

@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-8 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('doctor.dashboard.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('doctor.bookings.index') }}">Bookings</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Total Appointments: {{ $bookings->count() }}
                    </div>


                    <div class="card">
                        <form action="{{ route('doctor.bookings.index') }}" method="GET">
                            <div class="card-header">
                                Filter by Date: &nbsp;
                                <div class="row">
                                    <div class="col-md-10 col-sm-6">
                                        <input type="date" class="form-control datetimepicker-input" id="datepicker"
                                               name="date"
                                               placeholder=@isset($date) {{ $date }} @endisset>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="example">
                            <thead class="thead-light">
                            <tr>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Date of birth</th>
                                <th>Phone number</th>
                                <th>Doctor Name</th>

                            </tr>
                            </thead>
                            <tbody class="list">
                            @foreach($bookings as $key=>$booking)
                                <tr>
                                    <td>
                                        @if ($booking->status == 0)
                                            <a href="{{ route('doctor.bookings.update.status', [$booking->id]) }}">
                                                <button
                                                    class="btn btn-warning">Pending
                                                </button>
                                            </a>
                                        @else
                                            <a href="{{ route('doctor.bookings.update.status', [$booking->id]) }}">
                                                <button
                                                    class="btn btn-success">Checked-In
                                                </button>
                                            </a>
                                        @endif
                                    </td>
                                    <td>{{ $booking->date }}</td>
                                    <td>{{ $booking->time }}</td>
                                    <td>{{ $booking->first_name }} {{$booking->last_name  }}</td>
                                    <td>{{ $booking->email }}</td>
                                    <td>{{ $booking->address }}</td>
                                    <td>{{ $booking->birth_date }}</td>
                                    <td>{{ $booking->area_code }}-{{$booking->number  }}</td>
                                    <td>{{ $booking->doctor->fullName() }}</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript" charset="utf8"
            src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#example").dataTable();
        })
    </script>


@endpush
