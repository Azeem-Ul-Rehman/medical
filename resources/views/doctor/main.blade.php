<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Medical | @yield('title')</title>
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('backend/assets/img/brand/favicon.png') }}" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/nucleo/css/nucleo.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}"
          type="text/css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="{{ asset('backend/assets/css/style.css?v=1.2.0') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('backend/assets/css/toastr.min.css') }}">
    @stack('css')
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body>
<!-- Sidenav -->
@include('doctor.includes.sidebar')
<!-- Main content -->
<div class="main-content" id="panel">
    <!-- Topnav -->
@include('doctor.includes.header')
<!-- Header -->
    <!-- Header -->
@yield('banner')
<!-- Page content -->
    <div class="container-fluid mt--6">
    @yield('content')
    <!-- Footer -->
        @include('doctor.includes.footer')
    </div>
</div>

<!-- Core -->
<script src="{{ asset('backend/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('backend/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('backend/assets/vendor/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('backend/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('backend/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
<!-- Optional JS -->
<script src="{{ asset('backend/assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('backend/assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
<script src="{{ asset('backend/assets/js/custom.js?v=1.2.0') }}"></script>
<!-- Toastr -->
<script src="{{ asset('backend/assets/js/toastr.min.js') }}"></script>
<script type="text/javascript">

    function confirmDelete() {
        var r = confirm("Are you sure you want to perform this action");
        if (r === true) {
            return true;
        } else {
            return false;
        }
    }
</script>
<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif
</script>
@stack('models')
@stack('js')
</body>
</html>
