<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <p>Medical</p>
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('doctor/dashboard')) ? 'active' : '' }}"
                           href="{{ route('doctor.dashboard.index') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('doctor/patient')) ? 'active' : '' }}"
                           href="{{ route('doctor.patient.index') }}">
                            <i class="ni ni-single-02 text-primary"></i>
                            <span class="nav-link-text">Patient</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('doctor/fellow/patients')) ? 'active' : '' }}"
                           href="{{ route('doctor.fellow.patient.index') }}">
                            <i class="ni ni-user-run text-primary"></i>
                            <span class="nav-link-text">Fellow Doctor Patient</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('labeling/patients')) ? 'active' : '' }}"
                           href="{{ route('doctor.labeling.patient.index') }}">
                            <i class="ni ni-calendar-grid-58 text-primary"></i>
                            <span class="nav-link-text">Labeling Patients</span>
                        </a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{ (request()->is('doctor/appointment')) ? 'active' : '' }}"--}}
{{--                           href="{{ route('doctor.appointment.index') }}">--}}
{{--                            <i class="ni ni-calendar-grid-58 text-primary"></i>--}}
{{--                            <span class="nav-link-text">Appointment Schedule</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{ (request()->is('doctor/bookings')) ? 'active' : '' }}"--}}
{{--                           href="{{ route('doctor.bookings.index') }}">--}}
{{--                            <i class="ni ni-app text-primary"></i>--}}
{{--                            <span class="nav-link-text">Bookings</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </div>
    </div>
</nav>
