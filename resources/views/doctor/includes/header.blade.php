<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Navbar links -->
            <ul class="navbar-nav align-items-center  ml-md-auto ">
                <li class="nav-item d-xl-none">
                    <!-- Sidenav toggler -->
                    <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin"
                         data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </li>
                <li class="nav-item d-sm-none">
                    <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                        <i class="ni ni-zoom-split-in"></i>
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="ni ni-bell-55" id="notificationCount">{{ count($notifications) }}</i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                        <!-- List group -->
                        <div class="list-group list-group-flush">
                            @if(!empty($notifications) && count($notifications) > 0)
                                @foreach($notifications as $notification)
                                    @if(!is_null($notification->patient))
                                        <a href="javascript:void(0)" id="notification{{$notification->id}}"
                                           onclick="isRead('{{$notification->id}}')"
                                           class="list-group-item list-group-item-action">
                                            <div class="row align-items-center">
                                                <div class="col-auto">
                                                </div>
                                                <div class="col ml--2">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div>
                                                            <h4 class="mb-0 text-sm">{{ $notification->patient->fullName() }}</h4>
                                                        </div>
                                                        <div class="text-right text-muted">
                                                            <small>{{ $notification->created_at->diffForHumans() }}</small>
                                                        </div>
                                                    </div>
                                                    <p class="text-sm mb-0">Birthday Wishes has been successfully
                                                        sent.</p>
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endif

                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle"></span>
                            <div class="media-body  ml-2  d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold"> {{ auth()->user()->fullName() }}</span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu  dropdown-menu-right ">
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <div class="dropdown-divider"></div>

                        <a href="javascript:void(0);"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                           class="dropdown-item"><i class="ni ni-user-run"></i> <span>Logout</span></a>
                        <form id="logout-form" action="{{ route('logout') }}"
                              enctype="multipart/form-data"
                              method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<script>

    function isRead(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('ajax.notification.is-read') }}",
            data: {notification_id: id},
            success: function (response) {
                if (response.status == "success" && response.action == "true") {
                    var notificationCount = parseInt($('#notificationCount').text());
                    $('#notificationCount').text(notificationCount - 1);
                    $('#notification' + response.data).remove();
                }
            },
            error: function (error) {
                toastr['error']('something went wrong');
            }
        });
    }
</script>
