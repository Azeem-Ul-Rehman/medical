@extends('doctor.main')
@section('title','Dashboard')
@push('css')
    <link href="{{ asset('/') }}admin_assets/global/plugins/datatables/datatables.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('/') }}admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
          rel="stylesheet" type="text/css"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .table td, .table th {
            font-size: 12px;
            line-height: 2.42857 !important;
        }
    </style>

@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Patient</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('doctor.dashboard.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('doctor.patient.index') }}">Patient</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">

        <div class="col-md-12" style="margin-top:100px;">
            <div class="form-group row">
                <div class="col-md-12">
                    <h4>Filter</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-6">

                            <label for="is_recall"
                                   class="col-md-4 col-form-label text-md-left"><strong>Doctors List</strong></label>

                            <select
                                class="form-control @error('user_id') is-invalid @enderror" id="user_id" required
                                name="user_id" autocomplete="user_id">

                                <option value="">Select an option</option>
                                @if(!empty($doctors) && count($doctors) > 0)
                                    @foreach($doctors  as $doctor)
                                        <option value="{{$doctor->id}}">{{ $doctor->fullName() }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-sm btn-success" id="searchPatient"
                                    style="margin-top: 47px;padding: 10px;font-size: 15px;margin-right: 15px">
                                Submit to View Patients
                            </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="col-md-12">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Patient Record</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="user_datatable_ajax">
                            <thead class="thead-light">
                            <tr>
                                <th>Action</th>
                                <th>Doctor Name</th>
                                <th>Comanaged By</th>
                                <th>Is Recall</th>
                                <th>Location</th>
                                <th>Title</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Postal Code</th>
                                <th>Phone</th>
                                <th>XM Date</th>
                                <th>RX-L</th>
                                <th>RX-R</th>
                                <th>ADD</th>
                                <th>Birth Date</th>
                                <th>Comments</th>
                                <th>RX Date</th>

                            </tr>
                            </thead>
                            <tbody class="list">
                            </tbody>
                        </table>

                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('/') }}admin_assets/global/scripts/datatable.js" type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/datatables/datatables.min.js"
            type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>

    <script>

        $(function () {

            var oTable = '';
            $('#searchPatient').on('click', function (e) {
                if($('#user_id').find(":selected").val() == ''){
                    alert('Please select doctor to see patients.');
                }else{
                    oTable = $('#user_datatable_ajax').DataTable({
                        processing: true,
                        serverSide: true,
                        stateSave: true,
                        searching: true,
                        "pagingType": "simple",
                        retrieve: true,
                        "order": [[0, "asc"]],
                        ajax: {
                            url: '{!! route('doctor.fellow.patient.ajax.data') !!}',
                            data: function (d) {
                                d.user_id = $('#user_id').find(":selected").val();
                            }
                        }, columns: [
                            {data: 'action', name: 'action', orderable: false, searchable: false},
                            {data: 'doctor_name', name: 'doctor_name'},
                            {data: 'comanaged_by', name: 'comanaged_by'},
                            {data: 'is_recall', name: 'is_recall'},
                            {data: 'location', name: 'location'},
                            {data: 'title', name: 'title'},
                            {data: 'name', name: 'name'},
                            {data: 'email', name: 'email'},
                            {data: 'address', name: 'address'},
                            {data: 'city', name: 'city'},
                            {data: 'postal_code', name: 'postal_code'},
                            {data: 'primary_contact', name: 'primary_contact'},
                            {data: 'xm_date', name: 'xm_date'},
                            {data: 'rx_r', name: 'rx_r'},
                            {data: 'rx_l', name: 'rx_l'},
                            {data: 'add', name: 'add'},
                            {data: 'birth_date', name: 'birth_date'},
                            {data: 'comments', name: 'comments'},
                            {data: 'rx_date', name: 'rx_date'},

                        ]
                    });
                    oTable.draw();
                    e.preventDefault();
                }

            });


        });
    </script>


@endpush
