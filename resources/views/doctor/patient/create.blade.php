@extends('doctor.main')
@section('title','Dashboard')
@push('css')
    <style>
        .invalid-feedback {
            font-size: 80%;
            display: block;
            width: 100%;
            margin-top: .25rem;
            color: #fb6340;
        }
    </style>
@endpush
@section('banner')
    <div class="header pb-6 d-flex align-items-center"><!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>

    </div>
@endsection
@section('content')
    <div class="row" style="margin-top: 10px">
        <div class="col-xl-12 order-xl-1">
            <div class="card">

                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Create profile </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('doctor.patient.store') }}" autocomplete="off">
                        @csrf
                        <h6 class="heading-small text-muted mb-4">User information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group  @error('title') is-invalid @enderror">
                                        <label class="form-control-label" for="title">Title</label>
                                        <select id="title" class="form-control" name="title" required>
                                            <option value="Mr.">Mr.</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Dr.">Dr.</option>
                                            <option value="M.">M.</option>
                                            <option value="Mlle">Mlle</option>
                                            <option value="Mme">Mme</option>
                                        </select>
                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group  @error('first_name') is-invalid @enderror">
                                        <label class="form-control-label" for="first_name">First Name</label>
                                        <input type="text" id="first_name" value="{{ old('first_name') }}"
                                               class="form-control" name="first_name" required
                                               placeholder="First Name">
                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('last_name') is-invalid @enderror">
                                        <label class="form-control-label" for="last_name">Last Name</label>
                                        <input type="text" id="last_name" class="form-control" name="last_name" required
                                               placeholder="Last Name" value="{{ old('last_name') }}">
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="email">Email</label>
                                        <input type="email" id="email" class="form-control" name="email"
                                               placeholder="Email" value="{{ old('email') }}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="birth_date">Birth Date</label>
                                        <input type="date" id="birth_date" class="form-control" name="birth_date"
                                               required
                                               placeholder="Birth Date" value="{{ old('birth_date') }}">
                                        @error('birth_date')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="location">Location</label>
                                        <select id="location" class="form-control" name="location" required>
                                            @foreach($locations  as $location)
                                                <option value="{{$location->location}}">{{$location->location}}</option>
                                            @endforeach
                                        </select>
                                        @error('location')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="user_id">Doctors</label>
                                        <select id="user_id" class="form-control" name="user_id" required>
                                            @foreach($users  as $user)
                                                <option
                                                    value="{{$user->id}}" {{ ($user->id ==auth()->id()) ? 'selected' : '' }}>{{$user->fullName()}}</option>
                                            @endforeach
                                        </select>
                                        @error('user_id')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="is_recall">Is Recall</label>
                                        <select id="is_recall" class="form-control" name="is_recall" required>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                        @error('is_recall')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" id="comanaged_by" class="form-control" name="comanaged_by"
                                               required
                                               placeholder="Comanaged By" value="{{ old('comanaged_by') }}">
                                        @error('comanaged_by')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr class="my-4"/>
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Contact information</h6>
                        <div class="pl-lg-4" id="append">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="address">Address</label>
                                        <input id="address" class="form-control" placeholder="Home Address"
                                               name="address" value="{{ old('address') }}" required
                                               type="text">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="city">City</label>
                                        <select id="city" class="form-control" name="city" required>
                                            <option value="Mtl">Mtl</option>
                                            <option value="CSL">CSL</option>
                                            <option value="Hamp">Hamp</option>
                                            <option value="TMR">TMR</option>
                                            <option value="Westmount">Westmount</option>
                                            <option value="other">other</option>
                                        </select>
                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="postal_code">Postal code</label>
                                        <input type="text" id="postal_code" class="form-control" name="postal_code"
                                               required
                                               placeholder="Postal code" value="{{ old('postal_code') }}">
                                        @error('postal_code')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="contact">Nature</label>
                                        <input type="text" class="form-control" name="contact[]" value="Primary"
                                               readonly>
                                        @error('contact')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="type">Type</label>
                                        <select id="type" class="form-control" name="type[]" required>
                                            <option value="phone_number">Phone Number</option>
                                            <option value="work_number">Work Number</option>
                                            <option value="cell_number">Cell Number</option>
                                            <option value="home_number">Home Number</option>
                                        </select>
                                        @error('type')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="area_code">Area Code</label>
                                        <select id="area_code" class="form-control" name="area_code[]" required>
                                            @foreach($areacodes  as $areacode)
                                                <option value="{{$areacode->code}}">{{$areacode->code}}</option>
                                            @endforeach
                                        </select>
                                        @error('area_code')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="phone">Contact Number</label>
                                        <input type="text" id="number" class="form-control" name="number[]" required
                                               placeholder="Phone Number" value="{{ old('phone') }}">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div id="anotherOption">

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" onclick="addAnotherOption()"
                                            class="btn btn-primary pull-right" style="float: right;">Add more Contact
                                    </button>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4"/>
                        <!-- Description -->
                        <h6 class="heading-small text-muted mb-4">Other Information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="xm_date">XM Date</label>
                                        <input id="xm_date" class="form-control" placeholder="XM Date" required
                                               name="xm_date" value="{{ old('xm_date') }}"
                                               type="date">
                                        @error('xm_date')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="rx_r">RX-R </label>
                                        <input id="rx_r" class="form-control" placeholder="RX-R" required
                                               name="rx_r" value="{{ old('rx_r') }}"
                                               type="text">
                                        @error('rx_r')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label class="form-control-label" for="rx_l">RX-L</label>
                                        <input id="rx_l" class="form-control" placeholder="RX-L" required
                                               name="rx_l" value="{{ old('rx_l') }}"
                                               type="text">
                                        @error('rx_l')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="add">ADD</label>
                                        <select id="add" class="form-control" required
                                                name="add">
                                            @if(!empty($eyes) && count($eyes) > 0)
                                                @foreach($eyes as $eye)
                                                    @if(!is_null($eye->add) && $eye->add != 'null')
                                                        <option
                                                            value="{{$eye->add}}" {{ old('add') ? 'selected' :'' }}>{{$eye->add}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        @error('add')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="rx_date">RX Date</label>
                                        <input type="date" id="rx_date" class="form-control" name="rx_date"
                                               value="{{ old('rx_date') }}" required
                                               placeholder="RX Date">
                                        @error('rx_date')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Comments</label>
                                        <textarea rows="4" class="form-control" id="comments" name="comments" required
                                                  placeholder="A few words about you ..."> {{ old('comments') }}</textarea>
                                        @error('comments')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row align-items-center">
                                <div class="col-4 text-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')

    <script>
        var count = 1;

        function addAnotherOption() {

            var html = '';
            html += '<div class="row" id="optionsDiv">';
            html += '<div class="col-md-12">';
            html += '<span class="pull-right delete" style="float: right;color: red;">Remove</span>';
            html += '</div>';
            html += '<div class="col-md-2">';
            html += '<div class="form-group">';
            html += '<label class="form-control-label" for="contact">Nature</label>';
            html += '<input type="text" class="form-control" name="contact[]" value="Secondary" readonly>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-2">';
            html += '<div class="form-group">';
            html += '<label class="form-control-label" for="type">Type</label>';
            html += '<select id="type" class="form-control" name="type[]" required>';
            html += '<option value="phone_number">Phone Number</option>';
            html += '<option value="work_number">Work Number</option>';
            html += '<option value="cell_number">Cell Number</option>';
            html += '<option value="home_number">Home Number</option>';
            html += '</select>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-2">';
            html += '<div class="form-group">';
            html += '<label class="form-control-label" for="area_code">Area Code</label>';
            html += '<select id="area_code" class="form-control" name="area_code[]" required>';
            @if (isset($areacodes) && count($areacodes) > 0)
                @foreach ($areacodes  as $areacode)
                html += '<option value="{{$areacode->code}}">{{$areacode->code}}</option>';
            @endforeach
                @endif
                html += '</select>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-lg-6">';
            html += '<div class="form-group">';
            html += '<label class="form-control-label" for="phone">Contact Number</label>';
            html += '<input type="text" id="number" class="form-control" name="number[]" required  placeholder="Phone Number">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $('#anotherOption').append(html);
            count++;
        }

        $('#append').on('click', '.delete', function (e) {
            $(e.target).closest('#append #optionsDiv').remove();
            count--;
        });
    </script>


@endpush
