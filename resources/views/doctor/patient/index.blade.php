@extends('doctor.main')
@section('title','Dashboard')
@push('css')
    <link href="{{ asset('/') }}admin_assets/global/plugins/datatables/datatables.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('/') }}admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
          rel="stylesheet" type="text/css"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .table td, .table th {
            font-size: 12px;
            line-height: 2.42857 !important;
        }
    </style>
    {{--    <link rel="stylesheet" type="text/css"--}}
    {{--          href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">--}}

    <style>
        .mail-box {
            border-collapse: collapse;
            border-spacing: 0;
            display: table;
            table-layout: fixed;
            width: 100%;
        }

        .mail-box aside {
            display: table-cell;
            float: none;
            height: 100%;
            padding: 0;
            vertical-align: top;
        }

        .mail-box .sm-side {
            background: none repeat scroll 0 0 #e5e8ef;
            border-radius: 4px 0 0 4px;
            width: 25%;
        }

        .mail-box .lg-side {
            background: none repeat scroll 0 0 #fff;
            border-radius: 0 4px 4px 0;
            width: 75%;
        }

        .mail-box .sm-side .user-head {
            background: none repeat scroll 0 0 #00a8b3;
            border-radius: 4px 0 0;
            color: #fff;
            min-height: 80px;
            padding: 10px;
        }

        .user-head .inbox-avatar {
            float: left;
            width: 65px;
        }

        .user-head .inbox-avatar img {
            border-radius: 4px;
        }

        .user-head .user-name {
            display: inline-block;
            margin: 0 0 0 10px;
        }

        .user-head .user-name h5 {
            font-size: 14px;
            font-weight: 300;
            margin-bottom: 0;
            margin-top: 15px;
        }

        .user-head .user-name h5 a {
            color: #fff;
        }

        .user-head .user-name span a {
            color: #87e2e7;
            font-size: 12px;
        }

        a.mail-dropdown {
            background: none repeat scroll 0 0 #80d3d9;
            border-radius: 2px;
            color: #01a7b3;
            font-size: 10px;
            margin-top: 20px;
            padding: 3px 5px;
        }

        .inbox-body {
            padding: 20px;
        }

        .btn-compose {
            background: none repeat scroll 0 0 #ff6c60;
            color: #fff;
            padding: 12px 0;
            text-align: center;
            width: 100%;
        }

        .btn-compose:hover {
            background: none repeat scroll 0 0 #f5675c;
            color: #fff;
        }

        ul.inbox-nav {
            display: inline-block;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .inbox-divider {
            border-bottom: 1px solid #d5d8df;
        }

        ul.inbox-nav li {
            display: inline-block;
            line-height: 45px;
            width: 100%;
        }

        ul.inbox-nav li a {
            color: #6a6a6a;
            display: inline-block;
            line-height: 45px;
            padding: 0 20px;
            width: 100%;
        }

        ul.inbox-nav li a:hover, ul.inbox-nav li.active a, ul.inbox-nav li a:focus {
            background: none repeat scroll 0 0 #d5d7de;
            color: #6a6a6a;
        }

        ul.inbox-nav li a i {
            color: #6a6a6a;
            font-size: 16px;
            padding-right: 10px;
        }

        ul.inbox-nav li a span.label {
            margin-top: 13px;
        }

        ul.labels-info li h4 {
            color: #5c5c5e;
            font-size: 13px;
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 5px;
            text-transform: uppercase;
        }

        ul.labels-info li {
            margin: 0;
        }

        ul.labels-info li a {
            border-radius: 0;
            color: #6a6a6a;
        }

        ul.labels-info li a:hover, ul.labels-info li a:focus {
            background: none repeat scroll 0 0 #d5d7de;
            color: #6a6a6a;
        }

        ul.labels-info li a i {
            padding-right: 10px;
        }

        .nav.nav-pills.nav-stacked.labels-info p {
            color: #9d9f9e;
            font-size: 11px;
            margin-bottom: 0;
            padding: 0 22px;
        }

        .inbox-head {
            background: none repeat scroll 0 0 #41cac0;
            border-radius: 0 4px 0 0;
            color: #fff;
            min-height: 80px;
            padding: 20px;
        }

        .inbox-head h3 {
            display: inline-block;
            font-weight: 300;
            margin: 0;
            padding-top: 6px;
        }

        .inbox-head .sr-input {
            border: medium none;
            border-radius: 4px 0 0 4px;
            box-shadow: none;
            color: #8a8a8a;
            float: left;
            height: 40px;
            padding: 0 10px;
        }

        .inbox-head .sr-btn {
            background: none repeat scroll 0 0 #00a6b2;
            border: medium none;
            border-radius: 0 4px 4px 0;
            color: #fff;
            height: 40px;
            padding: 0 20px;
        }

        .table-inbox {
            border: 1px solid #d3d3d3;
            margin-bottom: 0;
        }

        .table-inbox tr td {
            padding: 12px !important;
        }

        .table-inbox tr td:hover {
            cursor: pointer;
        }

        .table-inbox tr td .fa-star.inbox-started, .table-inbox tr td .fa-star:hover {
            color: #f78a09;
        }

        .table-inbox tr td .fa-star {
            color: #d5d5d5;
        }

        .table-inbox tr.unread td {
            background: none repeat scroll 0 0 #f7f7f7;
            font-weight: 600;
        }

        ul.inbox-pagination {
            float: right;
        }

        ul.inbox-pagination li {
            float: left;
        }

        .mail-option {
            display: inline-block;
            margin-bottom: 10px;
            width: 100%;
        }

        .mail-option .chk-all, .mail-option .btn-group {
            margin-right: 5px;
        }

        .mail-option .chk-all, .mail-option .btn-group a.btn {
            background: none repeat scroll 0 0 #fcfcfc;
            border: 1px solid #e7e7e7;
            border-radius: 3px !important;
            color: #afafaf;
            display: inline-block;
            padding: 5px 10px;
        }

        .inbox-pagination a.np-btn {
            background: none repeat scroll 0 0 #fcfcfc;
            border: 1px solid #e7e7e7;
            border-radius: 3px !important;
            color: #afafaf;
            display: inline-block;
            padding: 5px 15px;
        }

        .mail-option .chk-all input[type="checkbox"] {
            margin-top: 0;
        }

        .mail-option .btn-group a.all {
            border: medium none;
            padding: 0;
        }

        .inbox-pagination a.np-btn {
            margin-left: 5px;
        }

        .inbox-pagination li span {
            display: inline-block;
            margin-right: 5px;
            margin-top: 7px;
        }

        .fileinput-button {
            background: none repeat scroll 0 0 #eeeeee;
            border: 1px solid #e6e6e6;
        }

        .inbox-body .modal .modal-body input, .inbox-body .modal .modal-body textarea {
            border: 1px solid #e6e6e6;
            box-shadow: none;
        }

        .btn-send, .btn-send:hover {
            background: none repeat scroll 0 0 #00a8b3;
            color: #fff;
        }

        .btn-send:hover {
            background: none repeat scroll 0 0 #009da7;
        }

        .modal-header h4.modal-title {
            font-family: "Open Sans", sans-serif;
            font-weight: 300;
        }

        .modal-body label {
            font-family: "Open Sans", sans-serif;
            font-weight: 400;
        }

        .heading-inbox h4 {
            border-bottom: 1px solid #ddd;
            color: #444;
            font-size: 18px;
            margin-top: 20px;
            padding-bottom: 10px;
        }

        .sender-info {
            margin-bottom: 20px;
        }

        .sender-info img {
            height: 30px;
            width: 30px;
        }

        .sender-dropdown {
            background: none repeat scroll 0 0 #eaeaea;
            color: #777;
            font-size: 10px;
            padding: 0 3px;
        }

        .view-mail a {
            color: #ff6c60;
        }

        .attachment-mail {
            margin-top: 30px;
        }

        .attachment-mail ul {
            display: inline-block;
            margin-bottom: 30px;
            width: 100%;
        }

        .attachment-mail ul li {
            float: left;
            margin-bottom: 10px;
            margin-right: 10px;
            width: 150px;
        }

        .attachment-mail ul li img {
            width: 100%;
        }

        .attachment-mail ul li span {
            float: right;
        }

        .attachment-mail .file-name {
            float: left;
        }

        .attachment-mail .links {
            display: inline-block;
            width: 100%;
        }

        .fileinput-button {
            float: left;
            margin-right: 4px;
            overflow: hidden;
            position: relative;
        }

        .fileinput-button input {
            cursor: pointer;
            direction: ltr;
            font-size: 23px;
            margin: 0;
            opacity: 0;
            position: absolute;
            right: 0;
            top: 0;
            transform: translate(-300px, 0px) scale(4);
        }

        .fileupload-buttonbar .btn, .fileupload-buttonbar .toggle {
            margin-bottom: 5px;
        }

        .files .progress {
            width: 200px;
        }

        .fileupload-processing .fileupload-loading {
            display: block;
        }

        * html .fileinput-button {
            line-height: 24px;
            margin: 1px -3px 0 0;
        }

        * + html .fileinput-button {
            margin: 1px 0 0;
            padding: 2px 15px;
        }

        @media (max-width: 767px) {
            .files .btn span {
                display: none;
            }

            .files .preview * {
                width: 40px;
            }

            .files .name * {
                display: inline-block;
                width: 80px;
                word-wrap: break-word;
            }

            .files .progress {
                width: 20px;
            }

            .files .delete {
                width: 60px;
            }
        }

        ul {
            list-style-type: none;
            padding: 0px;
            margin: 0px;
        }

    </style>

@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Patient</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('doctor.dashboard.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('doctor.patient.index') }}">Patient</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="{{ route('doctor.patient.create') }}" class="btn btn-sm btn-success"
                           style="padding: 13px;font-size: 15px;margin-right: 15px">Create</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">


        <div class="col-md-12">
            <form action="{{ route('doctor.patient.import.excel') }}" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-md-6">

                        <input type="file" name="file" class=" form-control">
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-sm btn-success"
                                style="padding: 10px;font-size: 15px;margin-right: 15px">Import Patient
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="margin-top: 10px;">

                <form action="javascript:void(0)" method="GET" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Filter</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-6">

                                    <label for="is_recall"
                                           class="col-md-4 col-form-label text-md-left"><strong>Is
                                            Recall:</strong></label>

                                    <select id="is_recall"
                                            class="form-control @error('is_recall') is-invalid @enderror"
                                            name="is_recall" autocomplete="is_recall">

                                        <option value="" selected>Select an option</option>
                                        <option
                                            value="yes" {{ (request()->get('is_recall') == 'yes') ? 'selected' : '' }}>
                                            Yes
                                        </option>
                                        <option
                                            value="no" {{ (request()->get('is_recall') == 'no') ? 'selected' : '' }}>
                                            No
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-sm btn-success" id="searchPatient"
                                            style="margin-top: 47px;padding: 10px;font-size: 15px;margin-right: 15px">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>


                </form>

            </div>
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Patient Record</h3>
                        <input type="button"
                               id='delete_record'
                               class="btn btn-sm ml-3 btn-danger float-right"
                               value='Bulk Delete'>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="example">
                            <thead class="thead-light">
                            <tr>
                                <th>Check All
                                    <input type="checkbox" class='checkall' id='checkall'>
                                </th>
                                <th>Action</th>
                                <th>Doctor Name</th>
                                <th>Comanaged By</th>
                                <th>Is Recall</th>
                                <th>Location</th>
                                <th>Title</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Postal Code</th>
                                <th>Phone</th>
                                <th>XM Date</th>
                                <th>RX-L</th>
                                <th>RX-R</th>
                                <th>ADD</th>
                                <th>Birth Date</th>
                                <th>Comments</th>
                                <th>RX Date</th>

                            </tr>
                            </thead>
                            <tbody class="list">
                            </tbody>
                        </table>

                        <hr>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('models')
    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade"
         style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Compose</h4>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>

                </div>
                <div class="modal-body">
                    <form role="form" class="form-horizontal" action="{{ route('doctor.send.email') }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="" name="patient_id" id="patient_id">
                        <input type="hidden" value="" name="doctor_email" id="doctor_email">
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="patient_email">To</label>
                            <div class="col-lg-12">
                                <input type="text" placeholder="" id="patient_email" name="patient_email"
                                       class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="subject">Subject</label>
                            <div class="col-lg-12">
                                <input type="text" placeholder="" id="subject" name="subject" class="form-control"
                                       required>
                            </div>
                        </div>
                        {{--                        <div class="form-group">--}}
                        {{--                            <label class="col-lg-2 control-label" for="message">Message</label>--}}
                        {{--                            <div class="col-lg-12">--}}
                        {{--                                <textarea rows="10" cols="30" class="form-control" id="message" name="message"--}}
                        {{--                                          required></textarea>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                                      <span class="btn green fileinput-button">
                                                        <i class="fa fa-plus fa fa-white"></i>
                                                        <span>Attachment</span>
                                                        <input type="file" name="file">
                                                      </span>
                                <button class="btn btn-send" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="printModal"
         class="modal fade"
         style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Print Document</h4>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>

                </div>
                <div class="modal-body">
                    <input type="hidden" value="" name="patient_id_print" id="patient_id_print">

                    <div class="form-group">
                        <label class="col-lg-6 control-label" for="printtype">Print Type</label>
                        <div class="col-lg-12">
                            <select name="printtype" id="printtype" class="form-control">
                                <option value="">Select Option</option>
                                <option value="1">Recall Letterhead 1</option>
                                <option value="2">Recall Letterhead 2</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="printButton">

                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endpush
@push('js')
    <style>
        /*.dataTables_info, .dataTables_paginate, .dataTables_length {*/
        /*    display: none;*/
        /*}*/
    </style>
    {{--    <script type="text/javascript" charset="utf8"--}}
    {{--            src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>--}}
    <script>
        // var dataTable;
        // $(function () {
        //     dataTable = $("#example").DataTable({
        //         // "paging": false,
        //         // "ordering": false,
        //         // "info": false,
        //     });
        // })
        // Check all
        $('#checkall').click(function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
            } else {
                $('.delete_check').prop('checked', false);
            }
        });

        // Delete record
        $('#delete_record').click(function () {

            var deleteids_arr = [];
            // Read all checked checkboxes
            $("input:checkbox[class=delete_check]:checked").each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {

                // Confirm alert
                var confirmdelete = confirm("Do you really want to Delete records?");
                if (confirmdelete == true) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    });
                    $.ajax({
                        url: '{{ route('doctor.patient.bulk.delete') }}',
                        type: 'post',
                        data: {deleteids_arr: deleteids_arr},
                        success: function (response) {
                            window.location.reload();
                        }
                    });
                }
            } else {
                alert('Please select items to delete.');
            }
        });

        // Checkbox checked
        function checkcheckbox() {
            debugger;

            // Total checkboxes
            var length = $('.delete_check').length;

            // Total checked checkboxes
            var totalchecked = 0;
            $('.delete_check').each(function () {
                if ($(this).is(':checked')) {
                    totalchecked += 1;
                }
            });

            // Checked unchecked checkbox
            if (totalchecked == length) {
                $("#checkall").prop('checked', true);
            } else {
                $('#checkall').prop('checked', false);
            }
        }

        $(document).on('click', '#sendEmail', function () {
            var id = $(this).data('id');
            var patientEmail = $(this).data('email');
            var doctorEmail = $(this).data('doctor-email');
            $('#patient_email').val(patientEmail);
            $('#patient_id').val(id);
            $('#doctor_email').val(doctorEmail);
            $('#myModal').modal('show');
        });
        $(document).on('click', '#printDocument', function () {
            var id = $(this).data('id');
            $('#patient_id_print').val(id);
            $('#printModal').modal('show');
        });
        $('#printtype').on('change', function () {
            var id = $('#patient_id_print').val();
            var html = '';
            if ($(this).val() == 1) {
                html = '<a class="btn btn-send float-right" href="/doctor/print/recall/letterhead/one/' + id + '">Print</a>';
                $('#printButton').html(html);
            } else if ($(this).val() == 2) {
                html = '<a class="btn btn-send float-right" href="/doctor/print/recall/letterhead/two/' + id + '">Print</a>';
                $('#printButton').html(html);
            } else {
                alert('Please select print type.');
            }
        })
    </script>

    <script src="{{ asset('/') }}admin_assets/global/scripts/datatable.js" type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/datatables/datatables.min.js"
            type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>

    <script>
        var oTable = '';
        $(function () {
            $('#searchPatient').on('click', function (e) {
                datatableDraw();
                e.preventDefault();

            });
            datatableDraw();


        });

        function datatableDraw() {
            oTable = $('#example').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: true,
                "pagingType": "simple",
                retrieve: true,
                "order": [[0, "asc"]],
                ajax: {
                    url: '{!! route('doctor.patient.ajax.data') !!}',
                    data: function (d) {
                        d.is_recall = $('#is_recall').find(":selected").val();
                    }
                }, columns: [
                    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'doctor_name', name: 'doctor_name'},
                    {data: 'comanaged_by', name: 'comanaged_by'},
                    {data: 'is_recall', name: 'is_recall'},
                    {data: 'location', name: 'location'},
                    {data: 'title', name: 'title'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'address', name: 'address'},
                    {data: 'city', name: 'city'},
                    {data: 'postal_code', name: 'postal_code'},
                    {data: 'primary_contact', name: 'primary_contact'},
                    {data: 'xm_date', name: 'xm_date'},
                    {data: 'rx_r', name: 'rx_r'},
                    {data: 'rx_l', name: 'rx_l'},
                    {data: 'add', name: 'add'},
                    {data: 'birth_date', name: 'birth_date'},
                    {data: 'comments', name: 'comments'},
                    {data: 'rx_date', name: 'rx_date'},

                ]
            });
            oTable.draw();
        }

        function confirmDeletePatient(patient_id) {
            if (confirmDelete()) {
                document.getElementById('delete_' + patient_id).submit();
            }
        }
    </script>



@endpush
