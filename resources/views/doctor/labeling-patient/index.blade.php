@extends('doctor.main')
@section('title','Dashboard')
@push('css')
    <link href="{{ asset('/') }}admin_assets/global/plugins/datatables/datatables.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('/') }}admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
          rel="stylesheet" type="text/css"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .table td, .table th {
            font-size: 12px;
            line-height: 2.42857 !important;
        }
    </style>

@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('doctor.dashboard.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('doctor.patient.index') }}">Labeling Patient</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">

        <div class="col-md-12" style="margin-top:100px;">
            <div class="form-group row">
                <div class="col-md-12">
                    <h4>Filter</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-3">

                            <label for="year"
                                   class="col-md-4 col-form-label text-md-left"><strong>Year</strong></label>
                            <select
                                class="form-control @error('year') is-invalid @enderror" id="year" required
                                name="year" autocomplete="year">

                                <option value="">Select an option</option>
                                @for($i=1990;$i<=2050;$i++)
                                    <option value="{{$i}}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-3">

                            <label for="month"
                                   class="col-md-4 col-form-label text-md-left"><strong>Month</strong></label>

                            <select
                                class="form-control @error('month') is-invalid @enderror" id="month" required
                                name="month" autocomplete="month">

                                <option value="">Select an option</option>
                                @for($i=1;$i<=12;$i++)
                                    <option value="{{$i}}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-3">

                            <label for="day"
                                   class="col-md-4 col-form-label text-md-left"><strong>Day</strong></label>

                            <select
                                class="form-control @error('day') is-invalid @enderror" id="day" required
                                name="day" autocomplete="day">

                                <option value="">Select an option</option>
                                @for($i=1;$i<=31;$i++)
                                    <option value="{{$i}}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-sm btn-success" id="searchPatient"
                                    style="margin-top: 47px;padding: 10px;font-size: 15px;margin-right: 15px">
                                Submit to View Patients
                            </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="col-md-12">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Labeling Patient Record</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="user_datatable_ajax">
                            <thead class="thead-light">
                            <tr>
                                <th>Doctor Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Address</th>
                                <th>Postal Code</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            </tbody>
                        </table>

                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('/') }}admin_assets/global/scripts/datatable.js" type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/datatables/datatables.min.js"
            type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script>

        $(function () {

            var oTable = '';
            $('#searchPatient').on('click', function (e) {
                if ($('#day').find(":selected").val() == '' && $('#month').find(":selected").val() == '' && $('#year').find(":selected").val() == '') {
                    alert('Please select at least one option to filter patients record');
                } else {
                    oTable = $('#user_datatable_ajax').DataTable({
                        processing: true,
                        serverSide: true,
                        stateSave: true,
                        // searching: true,
                        "pagingType": "simple",
                        retrieve: true,
                        dom: 'Bfrtip',
                        "pageLength": 20000,
                        "order": [[0, "asc"]],
                        "buttons": [
                            {
                                extend: 'excelHtml5',
                                title: "Labeling Patient",
                                filename: "Labeling Patient",
                                "createEmptyCells": true,
                                sheetName: "Labeling Patient",
                                text: "Export to xlsx",
                                action:newExportAction

                            }


                        ],
                        ajax: {
                            url: '{!! route('doctor.labeling.patient.ajax.data') !!}',
                            data: function (d) {
                                d.day = $('#day').find(":selected").val();
                                d.month = $('#month').find(":selected").val();
                                d.year = $('#year').find(":selected").val();
                            }
                        }, columns: [
                            {data: 'doctor_name', name: 'doctor_name'},
                            {data: 'first_name', name: 'first_name'},
                            {data: 'last_name', name: 'last_name'},
                            {data: 'address', name: 'address'},
                            {data: 'postal_code', name: 'postal_code'},

                        ]
                    });
                    oTable.draw();
                    e.preventDefault();
                }

            });

            var oldExportAction = function (self, e, dt, button, config) {
                if (button[0].className.indexOf('buttons-excel') >= 0) {
                    if ($.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)) {
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config);
                    }
                    else {
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                    }
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
            };

            var newExportAction = function (e, dt, button, config) {
                var self = this;
                var oldStart = dt.settings()[0]._iDisplayStart;

                dt.one('preXhr', function (e, s, data) {
                    // Just this once, load all data from the server...
                    data.start = 0;
                    data.length = 2147483647;

                    dt.one('preDraw', function (e, settings) {
                        // Call the original action function
                        oldExportAction(self, e, dt, button, config);

                        dt.one('preXhr', function (e, s, data) {
                            // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                            // Set the property to what it was before exporting.
                            settings._iDisplayStart = oldStart;
                            data.start = oldStart;
                        });

                        // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                        setTimeout(dt.ajax.reload, 0);

                        // Prevent rendering of the full data to the DOM
                        return false;
                    });
                });

                // Requery the server with the new one-time export settings
                dt.ajax.reload();
            };


        });
    </script>


@endpush
