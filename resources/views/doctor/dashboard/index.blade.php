@extends('doctor.main')
@section('title','Dashboard')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

    </style>
@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Default</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Default</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- Card stats -->
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Patients</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $patients }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="ni ni-active-40"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"></span>
                                    <span class="text-nowrap"></span>
                                </p>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-xl-3 col-md-6">--}}
{{--                        <div class="card card-stats">--}}
{{--                            <!-- Card body -->--}}
{{--                            <div class="card-body">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col">--}}
{{--                                        <h5 class="card-title text-uppercase text-muted mb-0">Check In appointments</h5>--}}
{{--                                        <span class="h2 font-weight-bold mb-0">{{ $confirmed_booking }}</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-auto">--}}
{{--                                        <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">--}}
{{--                                            <i class="ni ni-active-40"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <p class="mt-3 mb-0 text-sm">--}}
{{--                                    <span class="text-success mr-2"></span>--}}
{{--                                    <span class="text-nowrap"></span>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-3 col-md-6">--}}
{{--                        <div class="card card-stats">--}}
{{--                            <!-- Card body -->--}}
{{--                            <div class="card-body">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col">--}}
{{--                                        <h5 class="card-title text-uppercase text-muted mb-0">Pending Appointments</h5>--}}
{{--                                        <span class="h2 font-weight-bold mb-0">{{ $pending_booking }}</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-auto">--}}
{{--                                        <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow">--}}
{{--                                            <i class="ni ni-active-40"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <p class="mt-3 mb-0 text-sm">--}}
{{--                                    <span class="text-success mr-2"></span>--}}
{{--                                    <span class="text-nowrap"></span>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
@endsection
@push('js')
@endpush
