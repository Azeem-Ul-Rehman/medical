@extends('doctor.main')
@section('title','Appointment Schedule')
@push('css')
    <link rel="stylesheet" type="text/css"
          href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">

@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-8 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('doctor.dashboard.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('doctor.appointment.index') }}">Appointment
                                        Schedule</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-4 col-5 text-right">
                        <a href="{{ route('doctor.appointment.create') }}" class="btn btn-sm btn-success"
                           style="padding: 13px;font-size: 15px;margin-right: 15px">Create</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col">
                <div class="card">
                    <form action="{{ route('doctor.appointment.check') }}" method="post">
                        @csrf

                        <div class="card">
                            <div class="card-header">
                                Choose date
                                <br>
                                @if (isset($date))
                                    Your timetable for:
                                    {{ $date }}
                                @endif

                            </div>
                            <div class="card-body">
                                <input type="date" class="form-control datetimepicker-input" id="datepicker"
                                       name="date">
                                <br>
                                <button type="submit" class="btn btn-primary">Check</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if (Route::is('doctor.appointment.check'))
            <form action="{{ route('doctor.appointment.check.update') }}" method="post" style="width: 100%">
                @csrf

                <div class="col-md-12">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                Choose AM time
                                <span style="float: right">Check/Uncheck
                                <input type="checkbox"
                                       onclick=" for(c in document.getElementsByName('time[]')) document.getElementsByName('time[]').item(c).checked=this.checked">
                            </span>
                            </div>

                            <div class="card-body">

                                <table class="table table-striped">
                                    <tbody>
                                    <input type="hidden" name="appointmentId" value="{{ $appointmentId }} ">
                                    <tr>
                                        <th scope="row">1</th>

                                        <td><input type="checkbox" name="time[]" value="09:00am" @if (isset($times)){{ $times->contains('time', '09:00am') ? 'checked' : '' }}  @endif> 09:00am</td>
                                        <td><input type="checkbox" name="time[]" value="09:15am" @if (isset($times)){{ $times->contains('time', '09:15am') ? 'checked' : '' }}  @endif> 09:15am</td>
                                        <td><input type="checkbox" name="time[]" value="09:20am" @if (isset($times)){{ $times->contains('time', '09:20am') ? 'checked' : '' }}  @endif> 09:20am</td>
                                        <td><input type="checkbox" name="time[]" value="09:30am" @if (isset($times)){{ $times->contains('time', '09:30am') ? 'checked' : '' }}  @endif> 09:30am</td>
                                        <td><input type="checkbox" name="time[]" value="09:40am" @if (isset($times)){{ $times->contains('time', '09:40am') ? 'checked' : '' }}  @endif> 09:40am</td>
                                        <td><input type="checkbox" name="time[]" value="09:45am" @if (isset($times)){{ $times->contains('time', '09:45am') ? 'checked' : '' }}  @endif> 09:45am</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td><input type="checkbox" name="time[]" value="10:00am" @if (isset($times)){{ $times->contains('time', '10:00am') ? 'checked' : '' }}  @endif> 10:00am</td>
                                        <td><input type="checkbox" name="time[]" value="10:15am" @if (isset($times)){{ $times->contains('time', '10:15am') ? 'checked' : '' }}  @endif> 10:15am</td>
                                        <td><input type="checkbox" name="time[]" value="10:20am" @if (isset($times)){{ $times->contains('time', '10:20am') ? 'checked' : '' }}  @endif> 10:20am</td>
                                        <td><input type="checkbox" name="time[]" value="10:30am" @if (isset($times)){{ $times->contains('time', '10:30am') ? 'checked' : '' }}  @endif> 10:30am</td>
                                        <td><input type="checkbox" name="time[]" value="10:40am" @if (isset($times)){{ $times->contains('time', '10:40am') ? 'checked' : '' }}  @endif> 10:40am</td>
                                        <td><input type="checkbox" name="time[]" value="10:45am" @if (isset($times)){{ $times->contains('time', '10:45am') ? 'checked' : '' }}  @endif> 10:45am</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td><input type="checkbox" name="time[]" value="11:00am" @if (isset($times)){{ $times->contains('time', '11:00am') ? 'checked' : '' }}  @endif> 11:00am</td>
                                        <td><input type="checkbox" name="time[]" value="11:15am" @if (isset($times)){{ $times->contains('time', '11:15am') ? 'checked' : '' }}  @endif> 11:15am</td>
                                        <td><input type="checkbox" name="time[]" value="11:20am" @if (isset($times)){{ $times->contains('time', '11:20am') ? 'checked' : '' }}  @endif> 11:20am</td>
                                        <td><input type="checkbox" name="time[]" value="11:30am" @if (isset($times)){{ $times->contains('time', '11:30am') ? 'checked' : '' }}  @endif> 11:30am</td>
                                        <td><input type="checkbox" name="time[]" value="11:40am" @if (isset($times)){{ $times->contains('time', '11:40am') ? 'checked' : '' }}  @endif> 11:40am</td>
                                        <td><input type="checkbox" name="time[]" value="11:45am" @if (isset($times)){{ $times->contains('time', '11:45am') ? 'checked' : '' }}  @endif> 11:45am</td>
                                    </tr>



                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                Choose PM time
                            </div>
                            <div class="card-body">
                                <table class="table table-striped">

                                    <tbody>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td><input type="checkbox" name="time[]" value="12:00pm" @if (isset($times)){{ $times->contains('time', '12:00pm') ? 'checked' : '' }}  @endif> 12:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="12:15pm" @if (isset($times)){{ $times->contains('time', '12:15pm') ? 'checked' : '' }}  @endif> 12:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="12:20pm" @if (isset($times)){{ $times->contains('time', '12:20pm') ? 'checked' : '' }}  @endif> 12:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="12:30pm" @if (isset($times)){{ $times->contains('time', '12:30pm') ? 'checked' : '' }}  @endif> 12:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="12:40pm" @if (isset($times)){{ $times->contains('time', '12:40pm') ? 'checked' : '' }}  @endif> 12:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="12:45pm" @if (isset($times)){{ $times->contains('time', '12:45pm') ? 'checked' : '' }}  @endif> 12:45pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td><input type="checkbox" name="time[]" value="01:00pm" @if (isset($times)){{ $times->contains('time', '01:00pm') ? 'checked' : '' }}  @endif> 01:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="01:15pm" @if (isset($times)){{ $times->contains('time', '01:15pm') ? 'checked' : '' }}  @endif> 01:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="01:20pm" @if (isset($times)){{ $times->contains('time', '01:20pm') ? 'checked' : '' }}  @endif> 01:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="01:30pm" @if (isset($times)){{ $times->contains('time', '01:30pm') ? 'checked' : '' }}  @endif> 01:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="01:40pm" @if (isset($times)){{ $times->contains('time', '01:40pm') ? 'checked' : '' }}  @endif> 01:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="01:45pm" @if (isset($times)){{ $times->contains('time', '01:45pm') ? 'checked' : '' }}  @endif> 01:45pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td><input type="checkbox" name="time[]" value="02:00pm" @if (isset($times)){{ $times->contains('time', '02:00pm') ? 'checked' : '' }}  @endif> 02:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="02:15pm" @if (isset($times)){{ $times->contains('time', '02:15pm') ? 'checked' : '' }}  @endif> 02:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="02:20pm" @if (isset($times)){{ $times->contains('time', '02:20pm') ? 'checked' : '' }}  @endif> 02:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="02:30pm" @if (isset($times)){{ $times->contains('time', '02:30pm') ? 'checked' : '' }}  @endif> 02:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="02:40pm" @if (isset($times)){{ $times->contains('time', '02:40pm') ? 'checked' : '' }}  @endif> 02:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="02:45pm" @if (isset($times)){{ $times->contains('time', '02:45pm') ? 'checked' : '' }}  @endif> 02:45pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">7</th>
                                        <td><input type="checkbox" name="time[]" value="03:00pm" @if (isset($times)){{ $times->contains('time', '03:00pm') ? 'checked' : '' }}  @endif> 03:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="03:15pm" @if (isset($times)){{ $times->contains('time', '03:15pm') ? 'checked' : '' }}  @endif> 03:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="03:20pm" @if (isset($times)){{ $times->contains('time', '03:20pm') ? 'checked' : '' }}  @endif> 03:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="03:30pm" @if (isset($times)){{ $times->contains('time', '03:30pm') ? 'checked' : '' }}  @endif> 03:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="03:40pm" @if (isset($times)){{ $times->contains('time', '03:40pm') ? 'checked' : '' }}  @endif> 03:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="03:45pm" @if (isset($times)){{ $times->contains('time', '03:45pm') ? 'checked' : '' }}  @endif> 03:45pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">8</th>
                                        <td><input type="checkbox" name="time[]" value="04:00pm" @if (isset($times)){{ $times->contains('time', '04:00pm') ? 'checked' : '' }}  @endif> 04:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="04:15pm" @if (isset($times)){{ $times->contains('time', '04:15pm') ? 'checked' : '' }}  @endif> 04:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="04:20pm" @if (isset($times)){{ $times->contains('time', '04:20pm') ? 'checked' : '' }}  @endif> 04:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="04:30pm" @if (isset($times)){{ $times->contains('time', '04:30pm') ? 'checked' : '' }}  @endif> 04:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="04:40pm" @if (isset($times)){{ $times->contains('time', '04:40pm') ? 'checked' : '' }}  @endif> 04:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="04:45pm" @if (isset($times)){{ $times->contains('time', '04:45pm') ? 'checked' : '' }}  @endif> 04:45pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">9</th>
                                        <td><input type="checkbox" name="time[]" value="05:00pm" @if (isset($times)){{ $times->contains('time', '05:00pm') ? 'checked' : '' }}  @endif> 05:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="05:15pm" @if (isset($times)){{ $times->contains('time', '05:15pm') ? 'checked' : '' }}  @endif> 05:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="05:20pm" @if (isset($times)){{ $times->contains('time', '05:20pm') ? 'checked' : '' }}  @endif> 05:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="05:30pm" @if (isset($times)){{ $times->contains('time', '05:30pm') ? 'checked' : '' }}  @endif> 05:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="05:40pm" @if (isset($times)){{ $times->contains('time', '05:40pm') ? 'checked' : '' }}  @endif> 05:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="05:45pm" @if (isset($times)){{ $times->contains('time', '05:45pm') ? 'checked' : '' }}  @endif> 05:45pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">10</th>
                                        <td><input type="checkbox" name="time[]" value="06:00pm" @if (isset($times)){{ $times->contains('time', '06:00pm') ? 'checked' : '' }}  @endif> 06:00pm</td>
                                        <td><input type="checkbox" name="time[]" value="06:15pm" @if (isset($times)){{ $times->contains('time', '06:15pm') ? 'checked' : '' }}  @endif> 06:15pm</td>
                                        <td><input type="checkbox" name="time[]" value="06:20pm" @if (isset($times)){{ $times->contains('time', '06:20pm') ? 'checked' : '' }}  @endif> 06:20pm</td>
                                        <td><input type="checkbox" name="time[]" value="06:30pm" @if (isset($times)){{ $times->contains('time', '06:30pm') ? 'checked' : '' }}  @endif> 06:30pm</td>
                                        <td><input type="checkbox" name="time[]" value="06:40pm" @if (isset($times)){{ $times->contains('time', '06:40pm') ? 'checked' : '' }}  @endif> 06:40pm</td>
                                        <td><input type="checkbox" name="time[]" value="06:45pm" @if (isset($times)){{ $times->contains('time', '06:45pm') ? 'checked' : '' }}  @endif> 06:45pm</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
            {{-- Show app list --}}
        @else
            <div class="col-md-12">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Appointment Schedule</h3>
                        </div>
                        <!-- Light table -->
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush" id="example">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Creator</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">View & Update</th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                @foreach ($myAppointments as $appointment)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $appointment->doctor->fullName() }}</td>
                                        <td>{{ $appointment->date }}</td>
                                        <td>{{ $appointment->location }}</td>
                                        <td>
                                            <form action="{{ route('doctor.appointment.check') }}" method="post">@csrf
                                                <input type="hidden" name="date" value="{{ $appointment->date }}">
                                                <button type="submit" class="btn btn-primary">View & Update</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@push('js')
    <script type="text/javascript" charset="utf8"
            src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#example").dataTable();
        })
    </script>


@endpush
