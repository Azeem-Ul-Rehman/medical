@extends('doctor.main')
@section('title','Appointment Schedule')
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-8 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('doctor.dashboard.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('doctor.appointment.index') }}">Appointment
                                        Schedule</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-4 col-5 text-right">
                        <a href="{{ route('doctor.appointment.create') }}" class="btn btn-sm btn-success"
                           style="padding: 13px;font-size: 15px;margin-right: 15px">Create</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row" style="margin-top: 10px">
        <div class="col-xl-12 order-xl-1">
            <div class="card">

                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Choose your available time for appointments </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            @if (Session::has('message'))
                                <div class="alert bg-success alert-success text-white text-center" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            {{-- Error Message --}}
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>

                            @endforeach
                            {{-- Form --}}
                            <form action="{{ route('doctor.appointment.store') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3>Choose A Date</h3>
                                            </div>
                                            <div class="card-body">
                                                <input type="date" class="form-control datetimepicker-input"
                                                       id="datepicker"
                                                       name="date">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3>Choose location</h3>
                                            </div>
                                            <div class="card-body">

                                                <select id="location" class="form-control" name="location" required>
                                                    @foreach($locations  as $location)
                                                        <option
                                                            value="{{$location->location}}">{{$location->location}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        Choose AM Time
                                        <span class="ml-auto">Check/Uncheck
                            <input type="checkbox"
                                   onclick=" for(c in document.getElementsByName('time[]')) document.getElementsByName('time[]').item(c).checked=this.checked">
                        </span>
                                    </div>
                                    <div class="card-body">

                                        <table class="table table-striped">


                                            <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td><input type="checkbox" name="time[]" value="09:00am"> 09:00am</td>
                                                <td><input type="checkbox" name="time[]" value="09:15am"> 09:15am</td>
                                                <td><input type="checkbox" name="time[]" value="09:20am"> 09:20am</td>
                                                <td><input type="checkbox" name="time[]" value="09:30am"> 09:30am</td>
                                                <td><input type="checkbox" name="time[]" value="09:40am"> 09:40am</td>
                                                <td><input type="checkbox" name="time[]" value="09:45am"> 09:45am</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td><input type="checkbox" name="time[]" value="10:00am"> 10:00am</td>
                                                <td><input type="checkbox" name="time[]" value="10:15am"> 10:15am</td>
                                                <td><input type="checkbox" name="time[]" value="10:20am"> 10:20am</td>
                                                <td><input type="checkbox" name="time[]" value="10:30am"> 10:30am</td>
                                                <td><input type="checkbox" name="time[]" value="10:40am"> 10:40am</td>
                                                <td><input type="checkbox" name="time[]" value="10:45am"> 10:45am</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td><input type="checkbox" name="time[]" value="11:00am"> 11:00am</td>
                                                <td><input type="checkbox" name="time[]" value="11:15am"> 11:15am</td>
                                                <td><input type="checkbox" name="time[]" value="11:20am"> 11:20am</td>
                                                <td><input type="checkbox" name="time[]" value="11:30am"> 11:30am</td>
                                                <td><input type="checkbox" name="time[]" value="11:40am"> 11:40am</td>
                                                <td><input type="checkbox" name="time[]" value="11:45am"> 11:45am</td>
                                            </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        Choose PM Time
                                    </div>
                                    <div class="card-body">

                                        <table class="table table-striped">


                                            <tbody>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td><input type="checkbox" name="time[]" value="12:00pm"> 12:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="12:15pm"> 12:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="12:20pm"> 12:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="12:30pm"> 12:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="12:40pm"> 12:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="12:45pm"> 12:45pm</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td><input type="checkbox" name="time[]" value="01:00pm"> 01:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="01:15pm"> 01:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="01:20pm"> 01:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="01:30pm"> 01:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="01:40pm"> 01:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="01:45pm"> 01:45pm</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td><input type="checkbox" name="time[]" value="02:00pm"> 02:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="02:15pm"> 02:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="02:20pm"> 02:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="02:30pm"> 02:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="02:40pm"> 02:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="02:45pm"> 02:45pm</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">7</th>
                                                <td><input type="checkbox" name="time[]" value="03:00pm"> 03:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="03:15pm"> 03:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="03:20pm"> 03:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="03:30pm"> 03:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="03:40pm"> 03:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="03:45pm"> 03:45pm</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">8</th>
                                                <td><input type="checkbox" name="time[]" value="04:00pm"> 04:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="04:15pm"> 04:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="04:20pm"> 04:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="04:30pm"> 04:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="04:40pm"> 04:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="04:45pm"> 04:45pm</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">9</th>
                                                <td><input type="checkbox" name="time[]" value="05:00pm"> 05:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="05:15pm"> 05:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="05:20pm"> 05:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="05:30pm"> 05:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="05:40pm"> 05:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="05:45pm"> 05:45pm</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">10</th>
                                                <td><input type="checkbox" name="time[]" value="06:00pm"> 06:00pm</td>
                                                <td><input type="checkbox" name="time[]" value="06:15pm"> 06:15pm</td>
                                                <td><input type="checkbox" name="time[]" value="06:20pm"> 06:20pm</td>
                                                <td><input type="checkbox" name="time[]" value="06:30pm"> 06:30pm</td>
                                                <td><input type="checkbox" name="time[]" value="06:40pm"> 06:40pm</td>
                                                <td><input type="checkbox" name="time[]" value="06:45pm"> 06:45pm</td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <style type="text/css">
                            input[type="checkbox"] {
                                zoom: 1.1;

                            }

                            body {
                                font-size: 18px;
                            }

                        </style>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush
