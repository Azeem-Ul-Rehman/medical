@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-4 justify-content-center">
            <div class="col-md-8">
                <h1>Book your appointment today!</h1>
            </div>
        </div>

        {{-- Input --}}
        <form action="{{ url('/') }}" method="GET">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">Find Doctors</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-8">
                                <input type="date" name='date' id="datepicker" class='form-control'>
                            </div>
                            <div class="col-md-6 col-sm-4">
                                <button class="btn btn-primary" type="submit">Go</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        {{-- Display doctors --}}
        <div class="card">
            <div class="card-body">
                <div class="card-header">List of Doctors
                    Available: @if(isset($formatDate))   {{ $formatDate }} @else {{ date('Y-m-d') }} @endif
                </div>
                <div class="card-body table-responsive-sm">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Book</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($doctors as $key=>$doctor)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $doctor->doctor->fullName() }}</td>
                                <td>
                                    <a href="{{ route('create.appointment', [$doctor->doctor_id, $doctor->date]) }}">
                                        <button
                                            class="btn btn-primary">Appointment
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <td>No doctors available</td>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection
