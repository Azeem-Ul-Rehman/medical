@extends('layouts.master')
@section('title','Users')
@push('css')
    <link rel="stylesheet" type="text/css"
          href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">

@endpush
@section('banner')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Users</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-success" style="padding: 13px;font-size: 15px;margin-right: 15px">Create</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Users Record</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="example">
                            <thead class="thead-light">
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Email</th>

                            </tr>
                            </thead>
                            <tbody class="list">
                            @if(!empty($users) && count($users) >0)
                                @foreach($users as $user)
                                    <tr>
                                        <td nowrap style="width: 12%;">
                                            <a href="{{route('admin.users.edit',$user->id)}}" style="margin: 10px;"
                                               class="btn btn-sm btn-info pull-left ">Edit</a>
                                            <form method="post" action="{{ route('admin.users.destroy', $user->id) }}"
                                                  id="delete_{{ $user->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <a style="margin-left:10px;" class="btn btn-sm btn-danger m-left"
                                                   href="javascript:void(0)"
                                                   onclick="if(confirmDelete()){ document.getElementById('delete_<?=$user->id?>').submit(); }">
                                                    Delete
                                                </a>
                                            </form>
                                        </td>
                                        <td>{{ $user->fullName() }}</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript" charset="utf8"
            src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#example").dataTable();
        })
    </script>


@endpush
