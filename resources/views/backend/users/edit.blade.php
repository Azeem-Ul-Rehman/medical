@extends('layouts.master')
@section('title','Edit User')
@push('css')
    <style>
        .invalid-feedback {
            font-size: 80%;
            display: block;
            width: 100%;
            margin-top: .25rem;
            color: #fb6340;
        }
    </style>
@endpush
@section('banner')
    <div class="header pb-6 d-flex align-items-center"><!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>

    </div>
@endsection
@section('content')
    <div class="row" style="margin-top: 10px">
        <div class="col-xl-12 order-xl-1">
            <div class="card">

                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Edit User </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.users.update',$user->id) }}" autocomplete="off" enctype="multipart/form-data">
                        @method('patch')
                        @csrf
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group  @error('first_name') is-invalid @enderror">
                                        <label class="form-control-label" for="first_name">First Name</label>
                                        <input id="first_name" type="text"
                                               class="form-control @error('first_name') is-invalid @enderror"
                                               name="first_name" value="{{ old('first_name',$user->first_name) }}"
                                               autocomplete="first_name" autofocus>

                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('last_name') is-invalid @enderror">
                                        <label class="form-control-label" for="last_name">Last Name</label>
                                        <input id="last_name" type="text"
                                               class="form-control @error('last_name') is-invalid @enderror"
                                               name="last_name" value="{{ old('last_name',$user->last_name) }}"
                                               autocomplete="last_name" autofocus>

                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="email">Email</label>
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email"
                                               value="{{ old('email',$user->email) }}" autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group  @error('password') is-invalid @enderror">
                                        <label class="form-control-label" for="password">Password</label>
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               name="password"
                                               autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group  @error('password_confirmation') is-invalid @enderror">
                                        <label class="form-control-label" for="password_confirmation">Password
                                            Confirmation</label>
                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" autocomplete="new-password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group  @error('image') is-invalid @enderror">
                                        <label class="form-control-label" for="image">E-Signature</label>
                                        <input value="{{old('image')}}" type="file"
                                               class="form-control @error('image') is-invalid @enderror"
                                               onchange="readURL(this)" id="image"
                                               name="image" style="padding: 9px; cursor: pointer">
                                        <img width="300" height="200" class="img-thumbnail"   style="display:{{($user->e_signature) ? 'block' : 'none'}};"
                                             id="img"
                                             src="{{ asset('/uploads/signature/'.$user->e_signature) }}"
                                             alt="your image"/>

                                        @error('image')
                                        <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row align-items-center">
                                <div class="col-4 text-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')



@endpush
