@extends('layouts.master')
@section('title','Locations')
@push('css')
    <style>
        .invalid-feedback {
            font-size: 80%;
            display: block;
            width: 100%;
            margin-top: .25rem;
            color: #fb6340;
        }
    </style>
@endpush
@section('banner')
    <div class="header pb-6 d-flex align-items-center"><!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>
    </div>
@endsection
@section('content')
    <div class="row" style="margin-top: 10px">
        <div class="col-xl-12 order-xl-1">
            <div class="card">

                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Create Locations </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.locations.store') }}" autocomplete="off">
                        @csrf
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group  @error('location') is-invalid @enderror">
                                        <label class="form-control-label" for="location">Location</label>
                                        <input type="text" id="location" value="{{ old('location') }}" class="form-control" name="location"
                                               placeholder="Location">
                                        @error('location')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row align-items-center">
                                <div class="col-4 text-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')



@endpush
