@extends('layouts.master')
@section('title','Eye')
@push('css')
    <style>
        .invalid-feedback {
            font-size: 80%;
            display: block;
            width: 100%;
            margin-top: .25rem;
            color: #fb6340;
        }
    </style>
@endpush
@section('banner')
    <div class="header pb-6 d-flex align-items-center"><!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>
    </div>
@endsection
@section('content')
    <div class="row" style="margin-top: 10px">
        <div class="col-xl-12 order-xl-1">
            <div class="card">

                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Create Eye </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.eyes.store') }}" autocomplete="off">
                        @csrf
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group  @error('rx_r') is-invalid @enderror">
                                        <label class="form-control-label" for="rx_r">RX-R (+/-)</label>
                                        <input type="text" id="rx_r" value="{{ old('rx_r') }}" class="form-control" name="rx_r"
                                               placeholder="RX-R">
                                        @error('rx_r')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('rx_r_one') is-invalid @enderror">
                                        <label class="form-control-label" for="rx_r">RX-R (-)</label>
                                        <input type="text" id="rx_r_one" value="{{ old('rx_r_one') }}" class="form-control" name="rx_r_one"
                                               placeholder="RX-R One">
                                        @error('rx_r_one')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('rx_r_two') is-invalid @enderror">
                                        <label class="form-control-label" for="rx_r">RX-R (X)</label>
                                        <input type="text" id="rx_r_two" value="{{ old('rx_r_two') }}" class="form-control" name="rx_r_two"
                                               placeholder="RX-R Two">
                                        @error('rx_r_two')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('rx_l') is-invalid @enderror">
                                        <label class="form-control-label" for="rx_l">RX-L (+/-)</label>
                                        <input type="text" id="rx_l" class="form-control" name="rx_l"
                                               placeholder="RX-L" value="{{ old('rx_l') }}">
                                        @error('rx_l')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('rx_l_one') is-invalid @enderror">
                                        <label class="form-control-label" for="rx_l">RX-L (-)</label>
                                        <input type="text" id="rx_l_one" class="form-control" name="rx_l_one"
                                               placeholder="RX-L One" value="{{ old('rx_l_one') }}">
                                        @error('rx_l_one')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group  @error('rx_l_two') is-invalid @enderror">
                                        <label class="form-control-label" for="rx_l_two">RX-L (X)</label>
                                        <input type="text" id="rx_l_two" class="form-control" name="rx_l_two"
                                               placeholder="RX-L Two" value="{{ old('rx_l_two') }}">
                                        @error('rx_l_two')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="add">ADD</label>
                                        <input type="text" id="add" class="form-control" name="add"
                                               placeholder="ADD" value="{{ old('add') }}">
                                        @error('add')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row align-items-center">
                                <div class="col-4 text-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')



@endpush
