<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap" rel="stylesheet">


    <style type="text/css" rel="stylesheet" media="all">

        @import url(https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap);
        /* Base ------------------------------ */
        *:not(br):not(tr):not(html) {
            font-family: 'Exo';
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body, td, input, textarea, select {
            color: #003466 !important;
            margin: 0;
            font-family: Exo Text;
        }

        body, td, input, textarea, select, #loading {
            font-family: Exo Text !important;
        }

        body {
            width: 100% !important;
            height: 100%;
            margin: 0;
            line-height: 1.4;
            background-color: #F5F7F9;
            color: #839197;
            -webkit-text-size-adjust: none;
        }

        a {
            color: #414EF9;
        }

        /* Layout ------------------------------ */
        .email-wrapper {
            width: 100%;
            margin: 0;
            padding: 0;
            background-color: #F5F7F9;
        }

        .email-content {
            width: 100%;
            margin: 0;
            padding: 0;
        }

        /* Masthead ----------------------- */
        .email-masthead {
            padding: 25px 0;
            text-align: center;
        }

        .email-masthead_logo {
            max-width: 400px;
            border: 0;
        }

        .email-masthead_name {
            font-size: 16px;
            font-weight: bold;
            color: #839197;
            text-decoration: none;
            text-shadow: 0 1px 0 white;
        }

        /* Body ------------------------------ */
        .email-body {
            width: 100%;
            margin: 0;
            padding: 0;
            border-top: 1px solid #E7EAEC;
            border-bottom: 1px solid #E7EAEC;
            background-color: #FFFFFF;
        }

        .email-body_inner {
            width: 570px;
            margin: 0 auto;
            padding: 0;
        }

        .email-footer {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            text-align: center;
        }

        .email-footer p {
            color: #839197;
        }

        .body-action {
            width: 100%;
            margin: 30px auto;
            padding: 0;
            text-align: center;
        }

        .body-sub {
            margin-top: 25px;
            padding-top: 25px;
            border-top: 1px solid #E7EAEC;
        }

        .content-cell {
            padding: 35px;
        }

        .align-right {
            text-align: right;
        }

        /* Type ------------------------------ */
        h1 {
            margin-top: 0;
            color: #292E31;
            font-size: 19px;
            font-weight: bold;
            text-align: left;
        }

        h2 {
            margin-top: 0;
            color: #292E31;
            font-size: 16px;
            font-weight: bold;
            text-align: left;
        }

        h3 {
            margin-top: 0;
            color: #292E31;
            font-size: 14px;
            font-weight: bold;
            text-align: left;
        }

        p {
            margin-top: 0;
            color: #839197;
            font-size: 16px;
            line-height: 1.5em;
            text-align: left;
        }

        p.sub {
            font-size: 12px;
        }

        p.center {
            text-align: center;
        }

        /* Buttons ------------------------------ */
        .button {
            display: inline-block;
            width: 200px;
            background-color: #414EF9;
            border-radius: 3px;
            color: #ffffff;
            font-size: 15px;
            line-height: 45px;
            text-align: center;
            text-decoration: none;
            -webkit-text-size-adjust: none;
            mso-hide: all;
        }

        .button--green {
            background-color: #28DB67;
        }

        .button--red {
            background-color: #FF3665;
        }

        .button--blue {
            background-color: #414EF9;
        }

        /*Media Queries ------------------------------ */
        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }

        h2 {
            margin-top: 12px;
        }

        a.button {
            cursor: pointer;
            cursor: hand;
        }

        .verify-btn {
            font-family: 'Exo';
            font-size: 15px;
            box-sizing: border-box;
            border-radius: 3px;
            color: #fff !important;
            display: inline-block;
            text-decoration: none;
            background-color: #003466;
            border-top: 10px solid #003466;
            border-right: 18px solid #003466;
            border-bottom: 10px solid #003466;
            border-left: 18px solid #003466;
        }


        *:not(br):not(tr):not(html) {
            font-family: 'Exo';
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            color: #003466 !important;
        }


    </style>
    <style type="text/css">
        @font-face {
            font-family: Exo Text;
            src: url('https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap'); /* IE9 Compat
  Modes */
        url('https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap') format('woff'), /*
  Modern Browsers */
        url('https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap') format('truetype'),
        / Safari, Android, iOS /

        url('https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap') format('svg');
        / Legacy iOS /
        }

        p, h1, h2, h3, h4, span, ol, li, ul, b {
            font-family: 'Exo', sans-serif;
            font-weight: 400;
        }


    </style>
    <style type="text/css">
        @media screen {
            @font-face {
                font-family: 'Exo';
                font-style: normal;
                font-weight: 400;
                src: local('Exo Regular'), local('Exo-Regular'), url(https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap) format('woff');
            }
        }

        body {
            font-family: "Exo"
        }
    </style>

    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Exo:wght@400;500;600&display=swap');

        p, h1, h2, h3, h4, span, b ol, li, ul {
            font-family: 'Exo';
        }


    </style>
</head>
<body>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
{{--                <tr>--}}
{{--                    <td class="email-masthead content-cell" style="background: #5e72e4 !important;color:white;">--}}
{{--                        <span class="logodesc" style="color: #fff !important;">Spectorvision--}}
{{--              </span>--}}
{{--                    </td>--}}
{{--                </tr>--}}

                <tr>
                    <td class="email-masthead content-cell" style="background: #5e72e4 !important;color:white;">
                        <img src="{{ asset('backend/assets/img/logo-en.png') }}" style="width: 20% !important; height: auto; !important;">
                        <span class="logodesc">
                  <span class="logo-title"
                        style="
                        color:#000;
                        font-size: 26px;
                        display: block;
                        font-weight: bold;
                        margin-top: 5px;
                    "></span>

              </span>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell">
                                    <!-- Action -->
                                    <table class="body-action" align="center" width="100%" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td valign="top" align="left">
                                                <table class="p100"
                                                       style="margin: 0; mso-table-lspace: 0; mso-table-rspace: 0; padding: 0;"
                                                       cellspacing="0" cellpadding="0" border="0" align="left">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <table class="p100" id="fontColor"
                                                                   style="margin: 0; mso-table-lspace: 0; mso-table-rspace: 0; padding: 0; width: 600px;    color: #003466 !important;"
                                                                   width="600" cellspacing="0" cellpadding="0"
                                                                   border="0" align="left">
                                                                <tbody style=" color: #003466 !important">
                                                                <tr>
                                                                    <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left"
                                                                        class="editable_text text">


                                                                        <h1 style="">
                                                                            <font
                                                                                face="Tahoma"> Hi, {{ $patient->fullName() }}
                                                                            </font></h1>


                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left"
                                                                        class="editable_text text">


                                                                        <p><font face="Tahoma">


                                                                                According to our records, your last eye examination was {{ $patient->xm_date }}
                                                                                <br>
                                                                                It's time for your regular vision test and eye health examination. Please call our office to arrange
                                                                                for an appointment at your convenience.
                                                                                <br>
                                                                                <br>


                                                                                Bonjour, {{ $patient->fullName() }}
                                                                                <br>
                                                                                <br>

                                                                                Selon nos dossiers, votre denier examen visuel était le {{ $patient->xm_date }}
                                                                                <br>
                                                                                Il est temps pour votre examen de la vue et de l'examen de la santé oculaire. S'il vous plaît
                                                                                communiquez avec notre bureau pour prendre un rendez-vous à votre convenance.



                                                                            </font></p>


                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px; mso-line-height-rule: exactly;"
                                                                        valign="top" align="left"><font face="Tahoma">&nbsp;
                                                                        </font></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color: #000; font-size: 16px; font-weight: 600; letter-spacing: 0.02em; line-height: 23px; text-align: left; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left">
                                                                        <font face="Tahoma">
                                                                            Yours very truly,
                                                                            <br>
                                                                            Il nous fera plaisir de vous voir,
                                                                            <br>

                                                                            DR. ALLAN SPECTOR <br>
                                                                            DR. DONALD GORDON <br>
                                                                            DR. MARK GOLDSTEIN <br>
                                                                            DR. J.S. BEAUCHAMP
                                                                        </font>


                                                                    </td>
                                                                </tr>

                                                                <br>
                                                                <br>
                                                                <br>
                                                                <tr>
                                                                    <td style="color: #000; font-size: 16px; font-weight: 600; letter-spacing: 0.02em; line-height: 23px; text-align: left; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left">
                                                                        <img width="500" height="200" class="img-thumbnail"   style="display:{{($patient->user->e_signature) ? 'block' : 'none'}};object-fit: cover"
                                                                             id="img"
                                                                             src="{{ asset('/uploads/signature/'.$patient->user->e_signature) }}"
                                                                             alt="your image"/>


                                                                    </td>
                                                                </tr>



                                                                </tbody>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background: #5e72e4 !important;color:white;">
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-cell">
                                    <p class="sub center" style="color:#fff !important;">
                                        Support Team
                                        <br>
                                        <img src="{{ asset('backend/assets/img/logo-en.png') }}" style="width: 35% !important; height: auto; !important;">
{{--                                        <b style="color:#fff !important;">Spectorvision</b>--}}
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
