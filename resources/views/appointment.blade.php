@extends('layouts.app')
@push('css')
    <style>
        .invalid-feedback {
            display: block !important;
            font-size: 100% !important;

        }

        .pl-15 {
            padding-left: 15px;
        }

    </style>
    <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    <div class="loading" style="display: none !important;">Loading&#8230;</div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h4>Doctor Information</h4>
                        <img src="{{ asset('images') }}/blank.png" width="100px" style="border-radius: 50%;">
                        <br>
                        <p class="lead"> Name: {{ $user->fullName() }}</p>
                    </div>
                </div>

            </div>
            <div class="col-md-9">


                @if (Session::has('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                @endif

                @if (Session::has('errMessage'))
                    <div class="alert alert-danger">
                        {{ Session::get('errMessage') }}
                    </div>
                @endif

                <form action="{{ route('book.appointment') }}" method="post" onsubmit="return validate(event)">
                    @csrf
                    <div class="card">
                        <div class="card-header lead">Appointment Date: {{ $date }}</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label for="first_name" class="form-control-label">First Name</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" value=""
                                           placeholder="Enter first name">
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="last_name" class="form-control-label">Last Name</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" value=""
                                           placeholder="Enter last name">
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="email" class="form-control-label">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" value=""
                                           placeholder="Enter email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="col-md-8 form-group">
                                    <label for="address" class="form-control-label">Address</label>
                                    <textarea name="address" id="address" class="form-control"
                                              placeholder="Enter Address"></textarea>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="birth_date" class="form-control-label">Date of Birth</label>
                                    <input type="date" name="birth_date" id="birth_date" class="form-control" value=""
                                           placeholder="Enter Date of birth">
                                    @error('birth_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <input type="hidden" name="contact" value="Primary">
                                <div class="col-md-4 form-group">
                                    <label for="type" class="form-control-label">Contact Type</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="phone_number">Phone Number</option>
                                        <option value="work_number">Work Number</option>
                                        <option value="cell_number">Cell Number</option>
                                        <option value="home_number">Home Number</option>
                                    </select>

                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="area_code" class="form-control-label">Area Code</label>
                                    <select id="area_code" class="form-control" name="area_code">
                                        @foreach($areacodes  as $areacode)
                                            <option value="{{$areacode->code}}">{{$areacode->code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label class="form-control-label" for="phone">Contact Number</label>
                                    <input type="text" id="number" class="form-control" name="number"
                                           placeholder="Phone Number" value="{{ old('phone') }}">
                                    @error('number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <input type="hidden" name="location" id="location" value="">
                                @error('time')
                                <span class="invalid-feedback pl-15" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @foreach ($times as $time)
                                    <div class="col-md-3">
                                        <label class="btn btn-outline-primary btn-block">
                                            <input type="radio" name="time" value="{{ $time->time }}" id="time"
                                                   data-location="{{ $time->appointment->location }}">

                                            <span>{{ $time->time }} ({{$time->appointment->location}})</span>
                                        </label>
                                    </div>
                                    {{-- pass props to book app
                                    --}}
                                    <input type="hidden" name="doctorId" value="{{ $doctor_id }}">
                                    <input type="hidden" name="appointmentId" value="{{ $time->appointment_id }}">
                                    <input type="hidden" name="date" value="{{ $date }}">

                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Book Appointment</button>

                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('backend/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script>
        $(document).on('change', '#time', function () {
            $('#location').val($(this).data('location'))
        });

        function validate(e) {
            $(':input[type="submit"]').prop('disabled', true);
            $('.loading').show();
            return true;
        }
    </script>

@endpush
